/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.base.screen.Login', {
    extend: qx.ui.container.Composite,

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        /**
         * The form for login credentials.
         */
        form: null,

        __loginRequested: false,

        /**
         * Handler to send a login request.
         */
        requestLogin: function() {
            var me = this;
            var credentialItems = this.form.getItems();
            var name = credentialItems.Name.getValue();
            var password = credentialItems.Password.getValue();
            var request = new qx.io.request.Xhr('/login', 'POST');
            request.setRequestData({
                user: name,
                password: password
            });
            request.addListener('success', function(evt) {
                var req = evt.getTarget();
                me.fireDataEvent(
                    'loginSuccess',
                    req.getResponse());
                me.__loginRequested = false;
            });
            request.send();
        }
    },

    /*
    ***************************************************************************
     EVENTS
    ***************************************************************************
    */
    events: {
        /**
         * Fired when the login was successful.
         */
        loginSuccess: 'qx.event.type.Data',
        /**
         * Fired when the login was not successful.
         */
        loginFailure: 'qx.event.type.Event'
    },

    /**
    * Create a new login screen.
    */
    construct: function() {
        var me = this;
        me.base(arguments);
        me.setBackgroundColor('#6495ed');

        // Create the form
        me.form = new qx.ui.form.Form();
        var user = new qx.ui.form.TextField();
        user.addListener('appear', function() {
            var domElem = this.getContentElement().getDomElement();
            domElem.name = 'user';
            domElem.id = 'user';
            domElem.autocomplete = 'on';
        });
        me.form.add(user, 'Name');
        var passwd = new qx.ui.form.PasswordField();
        passwd.addListener('appear', function() {
            var domElem = this.getContentElement().getDomElement();
            domElem.name = 'password';
            domElem.id = 'password';
            domElem.autocomplete = 'on';
        });
        passwd.addListener('keypress', function(data) {
            if (data.getKeyCode() === 13 && !me.__loginRequested) {
                me.__loginRequested = true;
                me.requestLogin();
            }
        }, me);
        me.form.add(passwd, 'Password');
        var loginButton = new qx.ui.form.Button('Login');
        loginButton.addListener('execute', me.requestLogin, me);
        me.form.addButton(loginButton);
        var renderer = new qx.ui.form.renderer.Single(me.form);

        var formLayout = new qx.ui.layout.VBox();
        var headerLayout = new qx.ui.layout.Canvas();
        var headerContainer = new qx.ui.container.Composite(headerLayout);
        headerContainer.setHeight(100);
        var formContainer = new qx.ui.container.Composite(formLayout);
        headerContainer.add(new qx.ui.basic.Image('everydesk/icons/64x64/everydesk2-64x64.png'), {
            top: 18
        });
        headerContainer.add(new qx.ui.basic.Label('everydesk').set({
            font: new qx.bom.Font(30, ['tahoma', 'arial', 'verdana', 'sans-serif'])
        }), {
            left: 84,
            top: 35
        }
        );
        formContainer.add(headerContainer);
        formContainer.add(renderer);

        // Create the layout
        var hLayout = new qx.ui.layout.HBox();
        var vLayout = new qx.ui.layout.VBox();
        me.setLayout(vLayout);
        var hPage = new qx.ui.container.Composite(hLayout);

        // Insert spacer to have the form centered on the page
        hPage.add(new qx.ui.core.Spacer(), {
            flex: 1
        });
        hPage.add(formContainer);
        hPage.add(new qx.ui.core.Spacer(), {
            flex: 1
        });
        me.add(new qx.ui.core.Spacer(), {
            flex: 1
        });
        me.add(hPage);
        me.add(new qx.ui.core.Spacer(), {
            flex: 1
        });
    }
});

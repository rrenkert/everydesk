/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/background/*)
 * @asset(everydesk/icons/16x16/*)
 * @asset(everydesk/icons/32x32/*)
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.base.widget.MainMenu', {
    extend: qx.ui.container.Composite,

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        favorites: null,
        menus: null,
        __isFullScreen: false,

        logout: function(evt) {
            console.log('logout');
            var request = new everydesk.base.io.Xhr('/logout', 'GET');
            request.addListener('load', function() {
                if (request.getStatus() === 200) {
                    window.location.reload();
                }
                else {
                    //THROW AN ERROR!?
                }
            });
            request.setRequestHeader('X-EVERYDESK-SESSION', everydesk.base.system.Session.SESSIONKEY);
            request.send();
        },

        buildStarter: function(evt) {
            var me = this;
            me.menus.removeAll();
            var data = evt.getData();
            for (var i = 0; i < data.length; i++) {
                if (data[i].category) {
                    // Check if category exists
                    var categories = me.menus.getChildren();
                    var categoryExists = -1;
                    for (var j = 0; j < categories.length; j++) {
                        if (data[i].category ===
                                categories[j].getUserData('category')) {
                            categoryExists = j;
                            break;
                        }
                    }
                    if (categoryExists >= 0) {
                        categories[categoryExists].getMenu().add(
                            me.getGroupItem(data[i],
                            categories[categoryExists].getMenu()));
                    }
                    else {
                        me.menus.add(me.getCategoryItem(data[i]));
                    }
                }
                else {
                    me.menus.add(me.getStarterItem(data[i]));
                }
            }
        },

        buildFavorites: function(evt) {
            var me = this;
            me.favorites.removeAll();
            var data = evt.getData();
            console.log(data);
            for (var i = 0; i < data.length; i++) {
                if (data[i].group && data[i].group !== '') {
                    // Check if category exists
                    var groups = me.favorites.getChildren();
                    var groupExists = -1;
                    for (var j = 0; j < groups.length; j++) {
                        if (data[i].group ===
                                groups[j].getUserData('group')) {
                            groupExists = j;
                            break;
                        }
                    }
                    if (groupExists >= 0) {
                        var item = me.getFavoriteItem(data[i]);
                        groups[groupExists].getMenu().add(item);
                    }
                    else {
                        var g = me.getFavGroupItem(data[i]);
                        g.getMenu().add(me.getFavoriteItem(data[i]));
                        me.favorites.add(g);
                    }
                }
                else {
                    var item = me.getFavoriteItem(data[i]);
                    me.favorites.add(item);
                }
            }
        },

        getCategoryItem: function(data) {
            var me = this;
            var menu = new qx.ui.menu.Menu();
            menu.setPosition('right-top');
            menu.setOffset(0, 0, 0, 4);
            menu.setPaddingBottom(2);
            menu.setMinWidth(200);
            menu.set({
                decorator: 'mainsubmenu'
            });
            var item = new qx.ui.form.MenuButton(data.category, 'everydesk/icons/16x16/folder.png', menu);
            item.setHeight(30);
            item.setMargin(2, 3, 0, 3);
            item.setFont(new qx.bom.Font(12));
            item.setTextColor('rgb(250, 250, 250)');
            item.set({
                appearance: 'startermenu'
            });
            item.setUserData('category', data.category);
            menu.add(me.getGroupItem(data, menu));
            return item;
        },

        getFavGroupItem: function(data) {
            var menu = new qx.ui.menu.Menu();
            menu.setPosition('right-top');
            menu.setOffset(0, 0, 0, 4);
            menu.setPaddingBottom(2);
            menu.setMinWidth(200);
            menu.set({
                decorator: 'mainsubmenu'
            });
            var item = new qx.ui.form.MenuButton(data.group, 'everydesk/icons/16x16/folder.png', menu);
            item.setHeight(30);
            item.setMargin(2, 3, 0, 3);
            item.setFont(new qx.bom.Font(12));
            item.setTextColor('rgb(250, 250, 250)');
            item.set({
                appearance: 'startermenu'
            });
            item.setUserData('group', data.group);
            return item;
        },

        getGroupItem: function(data, parentMenu) {
            var me = this;
            if (!data.group) {
                return me.getStarterItem(data);
            }
            if (parentMenu) {
                var groups = parentMenu.getChildren();
                for (var i = 0; i < groups.length; i++) {
                    if (groups[i].getUserData('group') === data.group) {
                        groups[i].getMenu().add(me.getStarterItem(data));
                        return groups[i];
                    }
                }
            }
            var menu = new qx.ui.menu.Menu();
            menu.setPosition('right-top');
            menu.setOffset(-3, 0, 0, 3);
            menu.setPaddingBottom(3);
            menu.setMinWidth(200);
            menu.set({
                decorator: 'mainsubmenu'
            });
            var item = new qx.ui.menu.Button(data.group, 'everydesk/icons/16x16/folder.png', null, menu);
            item.setMargin(2, 3, 0, 3);
            item.setFont(new qx.bom.Font(12));
            item.setTextColor('rgb(250, 250, 250)');
            item.set({
                appearance: 'startermenu'
            });
            item.setUserData('group', data.group);
            menu.add(me.getStarterItem(data));
            return item;
        },

        getFavoriteItem: function(data) {
            var item;
            var icon;
            if (data.type.name === 'file') {
                if (data.icon) {
                    icon = data.icon;
                }
                else {
                    icon = 'everydesk/icons/16x16/monitor_link.png';
                }
            }
            else if (data.type.name === 'url') {
                if (data.icon) {
                    icon = data.icon;
                }
                else {
                    icon = 'everydesk/icons/16x16/monitor_link.png';
                }
            }
            if (data.group) {
                item = new qx.ui.menu.Button(data.name, icon, null);
                item.setMargin(2, 3, 0, 3);
            }
            else {
                item = new qx.ui.form.Button(data.name, icon, null);
                item.setMaxHeight(30);
                item.setMargin(2, 3, 0, 3);
            }
            item.setFont(new qx.bom.Font(12));
            item.setTextColor('rgb(250, 250, 250)');
            item.set({
                appearance: 'starterbutton'
            });
            item.getChildControl('icon', true).setHeight(16);
            item.getChildControl('icon', true).setWidth(16);
            item.getChildControl('icon', true).setScale(true);
            item.setUserData('type', data.type);
            item.setUserData('endpoint', data.endpoint);
            item.setUserData('name', data.name);
            item.setUserData('uri', data.uri);
            item.addListener('execute', function() {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'favstart', {
                        type: this.getUserData('type'),
                        endpoint: this.getUserData('endpoint'),
                        name: this.getUserData('name'),
                        uri: this.getUserData('uri')
                    }
                ));
            }, item);
            return item;
        },

        getStarterItem: function(data) {
            var item;
            if (!data.category) {
                item = new qx.ui.form.Button(data.name, 'everydesk/icons/16x16/' + data.icon, null);
                item.setHeight(30);
                item.setMargin(2, 3, 0, 3);
            }
            else {
                item = new qx.ui.menu.Button(data.name, 'everydesk/icons/16x16/' + data.icon, null);
                item.setMargin(2, 3, 0, 3);
            }
            item.setFont(new qx.bom.Font(12));
            item.setTextColor('rgb(250, 250, 250)');
            item.set({
                appearance: 'starterbutton'
            });
            item.setUserData('type', data.type);
            item.setUserData('endpoint', data.endpoint);
            item.setUserData('icon', data.icon);
            item.setUserData('name', data.name);
            item.addListener('execute', function() {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'appstart', {
                        type: this.getUserData('type'),
                        endpoint: this.getUserData('endpoint'),
                        name: this.getUserData('name'),
                        icon: this.getUserData('icon'),
                        file: null
                    }
                ));
            }, item);
            return item;
        }
    },

    /*
    ***************************************************************************
     EVENTS
    ***************************************************************************
    */
    events: {
    },

    /**
    * Create a new login screen.
    */
    construct: function() {
        var me = this;
        me.base(arguments);
        me.setLayout(new qx.ui.layout.VBox());
        me.setWidth(250);
        me.set({
            decorator: 'mainmenu'
        });
        var favHeaderContainer = new qx.ui.container.Composite();
        var favHeaderLayout = new qx.ui.layout.HBox();
        favHeaderContainer.setLayout(favHeaderLayout);
        var labelFav = new qx.ui.basic.Atom(
            'Favoriten',
            'everydesk/icons/16x16/star.png');
        labelFav.setFont(new qx.bom.Font(12));
        labelFav.setTextColor('rgb(250, 250, 250)');
        labelFav.setMargin(10, 10, 5, 10);
        favHeaderContainer.add(labelFav, {flex: 1});
        var favoriteSettings = new qx.ui.basic.Atom(null, 'everydesk/icons/16x16/cog.png');
        favoriteSettings.setMargin(10, 10, 5, 10);
        favoriteSettings.addListener('click', function() {
            qx.event.message.Bus.dispatch(new qx.event.message.Message(
                'appstart', {
                    endpoint: 'everydesk.plugin.favoritemanager.FavoriteManager',
                    name: 'Favorites',
                    icon: 'star.png',
                    type: {
                        id: 1,
                        name: 'app'
                    },
                    file: null
                }
            ));
        });
        var newFavorite = new qx.ui.basic.Atom(null, 'everydesk/icons/16x16/add.png');
        newFavorite.setMargin(10, 10, 5, 0);
        newFavorite.addListener('click', function() {
            qx.event.message.Bus.dispatch(new qx.event.message.Message(
                'appstart', {
                    endpoint: 'everydesk.plugin.favoritemanager.NewFavorite',
                    name: 'New Favorite',
                    icon: 'star.png',
                    type: {
                        id: 2,
                        name: 'dialog'
                    },
                    file: null
                }
            ));
        });
        var favExtension = new qx.ui.basic.Atom(null, 'everydesk/icons/16x16/star.png');
        favExtension.setMargin(10, 10, 5, 0);
        favExtension.addListener('click', function() {
            var request = new qx.bom.request.Xhr();
            request.open('GET', '/file/extension');
            request.responseType = 'arraybuffer';
            request.setRequestHeader('X-EVERYDESK-SESSION', everydesk.base.system.Session.SESSIONKEY);
            request.setRequestHeader('Content-Type', 'application/zip');
            request.onload = function() {
                var type = 'application/zip';
                var name = 'everydesk-0.2-an+fx.xpi';
                var content = request.getRequest().response;
                var blob = new Blob([content], {type: type});
                saveAs(blob, name);
            };
            request.send();
        });
        favHeaderContainer.add(favExtension);
        favHeaderContainer.add(favoriteSettings);
        favHeaderContainer.add(newFavorite);
        me.add(favHeaderContainer);

        me.favorites = new qx.ui.container.SlideBar('vertical');
        me.favorites.setLayout(new qx.ui.layout.VBox(5));
        me.add(me.favorites, {
            height: '40%'
        });

        var separator = new qx.ui.menu.Separator();
        separator.set({
            decorator: 'mainmenu-separator'
        });
        me.add(separator);

        var labelMenu = new qx.ui.basic.Atom(
            'Anwendungen',
            'everydesk/icons/16x16/application.png');
        labelMenu.setFont(new qx.bom.Font(12));
        labelMenu.setTextColor('rgb(250, 250, 250)');
        labelMenu.setMargin(10, 10, 5, 10);
        me.add(labelMenu);

        me.menus = new qx.ui.container.SlideBar('vertical');
        me.menus.setLayout(new qx.ui.layout.VBox(5));
        me.add(me.menus, {
            flex: 1
        });

        var separator2 = new qx.ui.menu.Separator();
        separator2.set({
            decorator: 'mainmenu-separator'
        });
        me.add(separator2);

        me.systemActions =
            new qx.ui.container.Composite(new qx.ui.layout.HBox(5));
        me.systemActions.setMargin(5);
        var logout = new qx.ui.form.Button(
            null,
            'everydesk/icons/32x32/door_in.png');
        logout.set({
            appearance: 'mainmenubutton'
        });
        logout.addListener('execute', me.logout);
        me.systemActions.add(logout);

        var lock = new qx.ui.form.Button(
            null,
            'everydesk/icons/32x32/lock.png');
        lock.set({
            appearance: 'mainmenubutton'
        });
        me.systemActions.add(lock);

        var saveSession = new qx.ui.form.Button(
            null,
            'everydesk/icons/32x32/plus_circle_frame.png');
        saveSession.set({
            appearance: 'mainmenubutton'
        });
        saveSession.addListener('execute', function(evt) {
            var wsm = everydesk.base.system.WorkspaceManager.getInstance();
            wsm.saveCurrent();
        });
        me.systemActions.add(saveSession);

        var refresh = new qx.ui.form.Button(
            null,
            'everydesk/icons/32x32/arrow_refresh.png');
        refresh.set({
            appearance: 'mainmenubutton'
        });
        me.systemActions.add(refresh);

        me.add(me.systemActions);
        qx.event.message.Bus.subscribe('loadsuccessStarter', me.buildStarter, me);
        qx.event.message.Bus.subscribe('loadsuccessFavorites', me.buildFavorites, me);
    }
});

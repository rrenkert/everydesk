/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.base.system.FavoritesManager', {
    extend: qx.core.Object,
    type: 'singleton',

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        favorites: null,

        load: function() {
            var me = this;
            qx.event.message.Bus.dispatch(new qx.event.message.Message(
                'actionstart', {
                    type: 'favorite',
                    name: 'favorite',
                    icon: 'everydesk/icons/16x16/star.png'
                }
            ));
            var request = new everydesk.base.io.Xhr('/favorites', 'GET');
            request.setRequestHeader(
                'X-EVERYDESK-SESSION',
                everydesk.base.system.Session.SESSIONKEY);
            request.addListener('success', function(evt) {
                me.favorites = evt.getTarget().getResponse();
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'favorite',
                        name: 'favorite',
                        icon: 'everydesk/icons/16x16/star.png'
                    }
                ));
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'loadsuccessFavorites',
                    me.favorites));
            });
            request.send();
        },

        getFavorites: function() {
            return this.favorites;
        },

        addFavorite: function(content) {
            console.log(content);
            qx.event.message.Bus.dispatch(new qx.event.message.Message(
                'actionstart', {
                    type: 'favorite',
                    name: 'favorite',
                    icon: 'everydesk/icons/16x16/star.png'
                }
            ));
            var me = this;
            var request = new everydesk.base.io.Xhr('/favorites', 'POST');
            request.setRequestHeader(
                'X-EVERYDESK-SESSION',
                everydesk.base.system.Session.SESSIONKEY);
            request.setRequestHeader(
                'Content-Type', 'application/json'
            );
            request.addListener('success', function(evt) {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'favorite',
                        name: 'favorite',
                        icon: 'everydesk/icons/16x16/star.png'
                    }
                ));
                me.load();
                console.log(evt);
            });
            request.addListener('fail', function() {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'favorite',
                        name: 'favorite',
                        icon: 'everydesk/icons/16x16/star.png'
                    }
                ));
            });
            request.setRequestData(content);
            request.send();
        },

        updateFavorite: function(content) {
            var me = this;
            qx.event.message.Bus.dispatch(new qx.event.message.Message(
                'actionstart', {
                    type: 'favorite',
                    name: 'favorite',
                    icon: 'everydesk/icons/16x16/star.png'
                }
            ));
            var request = new everydesk.base.io.Xhr('/favorites', 'PUT');
            request.setRequestHeader(
                'X-EVERYDESK-SESSION',
                everydesk.base.system.Session.SESSIONKEY);
            request.setRequestHeader(
                'Content-Type', 'application/json'
            );
            request.addListener('success', function(evt) {
                console.log(evt);
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'favorite',
                        name: 'favorite',
                        icon: 'everydesk/icons/16x16/star.png'
                    }
                ));
                me.load();
            });
            request.addListener('fail', function() {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'favorite',
                        name: 'favorite',
                        icon: 'everydesk/icons/16x16/star.png'
                    }
                ));
            });
            request.setRequestData(content);
            request.send();
        },

        deleteFavorite: function(content) {
            var me = this;
            qx.event.message.Bus.dispatch(new qx.event.message.Message(
                'actionstart', {
                    type: 'favorite',
                    name: 'favorite',
                    icon: 'everydesk/icons/16x16/star.png'
                }
            ));
            var request = new everydesk.base.io.Xhr('/favorites/' + content.id, 'DELETE');
            request.setRequestHeader(
                'X-EVERYDESK-SESSION',
                everydesk.base.system.Session.SESSIONKEY);
            request.addListener('success', function(evt) {
                console.log(evt);
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'favorite',
                        name: 'favorite',
                        icon: 'everydesk/icons/16x16/star.png'
                    }
                ));
                me.load();
            });
            request.addListener('fail', function() {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'favorite',
                        name: 'favorite',
                        icon: 'everydesk/icons/16x16/star.png'
                    }
                ));
            });
            request.send();
        }
    },

    /*
    ***************************************************************************
     EVENTS
    ***************************************************************************
    */
    events: {
        /**
         * Fired when the login was successful.
         */
        loadsuccess: 'qx.event.type.Data',
        /**
         * Fired when the login was not successful.
         */
        loadfailure: 'qx.event.type.Event'
    },

    construct: function() {
        this.base(arguments);
        var me = this;
        console.log("add event listener");
    }
});

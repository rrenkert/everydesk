/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.base.system.WorkspaceManager', {
    extend: qx.core.Object,
    type: 'singleton',

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {

        /*
        * {
        *   $WORKSPACENAME: {
        *       active: '',
        *       states: [{
        *           plugin: '',
        *           state: {}
        *       }]
        *   },
        *
        * }
        */
        __workspaces: null,

        __current: 'default',

        load: function() {
            var me = this;
            var request = new qx.io.request.Xhr('/workspace', 'GET');
            request.setRequestHeader(
                'X-EVERYDESK-SESSION',
                everydesk.base.system.Session.SESSIONKEY);
            request.addListener('success', function(evt) {
                me.__workspaces = evt.getTarget().getResponse();
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'workspaceloaded',
                    me.__workspaces));
            });
            request.send();
        },

        saveWorkspaces: function() {
            var tm = everydesk.base.system.TaskManager.getInstance();
            var tasks = tm.getTasks();
        },

        addWorkspace: function(workspace) {
            return workspace;
        },

        removeWorkspace: function(name) {
            return name;
        },

        getCurrentWorkspace: function() {
            return this.__current;
        },

        saveCurrent: function() {
            var me = this;
            var tm = everydesk.base.system.TaskManager.getInstance();
            var tasks = tm.getTasks();
            var apps = [];
            for (var i = 0; i < tasks.length; i++) {
                var app = {
                    endpoint: tasks[i].getEndpoint(),
                    name: tasks[i].getName(),
                    type: tasks[i].getType(),
                    icon: tasks[i].getIcon(),
                    state: tasks[i].getUi().getState()
                };
                apps.push(app);
            }

            var ws = {
                name: 'default',
                state: JSON.stringify(apps)
            };
            var request = new everydesk.base.io.Xhr('/workspace', 'POST');
            request.setRequestHeader(
                'X-EVERYDESK-SESSION',
                everydesk.base.system.Session.SESSIONKEY);
            request.setRequestHeader(
                'Content-Type', 'application/json'
            );
            request.addListener('success', function(evt) {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'workspace',
                        name: 'workspace',
                        icon: 'everydesk/icons/16x16/monitor_window.png'
                    }
                ));
            });
            request.addListener('fail', function() {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'workspace',
                        name: 'workspace',
                        icon: 'everydesk/icons/16x16/monitor_window.png'
                    }
                ));
            });
            request.setRequestData(ws);
            request.send();
        },

        getWorkspace: function(id) {
            for (var i = 0; i < this.__workspaces.length; i++) {
                if (this.__workspaces[i].id === id) {
                    return this.__workspaces[i];
                }
            }
        },
        restoreWorkspace: function(workspace) {
            /**
             * workspace: {
             *  name: 'theName'
             *  id: $ID
             *  apps: [{
             *      endpoint: 'the.end.point'
             *      state: {
             *          height: $HEIGHT,
             *          width: $WIDTH,
             *          ...
             *      }
             *  }]
             * }
             */
            var states = JSON.parse(workspace.state);
            for (var i = 0; i < states.length; i++) {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'appstart', {
                        type: {name: states[i].type},
                        endpoint: states[i].endpoint,
                        name: states[i].name,
                        icon: states[i].icon,
                        file: null,
                        state: states[i].state
                    }
                ));
            }
        }
    },

    /*
    ***************************************************************************
     EVENTS
    ***************************************************************************
    */
    events: {
        /**
         * Fired when the login was successful.
         */
        loadsuccess: 'qx.event.type.Data',
        /**
         * Fired when the login was not successful.
         */
        loadfailure: 'qx.event.type.Event'
    }
});

/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.base.system.SettingsManager', {
    extend: qx.core.Object,
    type: 'singleton',

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        settings: null,

        load: function() {
            var me = this;
            qx.event.message.Bus.dispatch(new qx.event.message.Message(
                'actionstart', {
                    type: 'settings',
                    name: 'settings',
                    icon: 'everydesk/icons/16x16/cog.png'
                }
            ));
            var request = new everydesk.base.io.Xhr('/settings', 'GET');
            request.setRequestHeader(
                'X-EVERYDESK-SESSION',
                everydesk.base.system.Session.SESSIONKEY);
            request.addListener('success', function(evt) {
                me.settings = evt.getTarget().getResponse();
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'settings',
                        name: 'settings',
                        icon: 'everydesk/icons/16x16/cog.png'
                    }
                ));
                console.log(me.settings);
                if (me.settings.background !== '') {
                    qx.event.message.Bus.dispatch(new qx.event.message.Message(
                        'changebackground', {
                            src: me.settings.background
                        }
                    ));
                }
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'loadsuccessSettings',
                    me.settings));
            });
            request.send();
        },

        getSettings: function() {
            return this.settings;
        },

        updateSettings: function(content) {
            console.log(content);
            qx.event.message.Bus.dispatch(new qx.event.message.Message(
                'actionstart', {
                    type: 'settings',
                    name: 'settings',
                    icon: 'everydesk/icons/16x16/cog.png'
                }
            ));
            var me = this;
            var request = new everydesk.base.io.Xhr('/settings', 'POST');
            request.setRequestHeader(
                'X-EVERYDESK-SESSION',
                everydesk.base.system.Session.SESSIONKEY);
            request.setRequestHeader(
                'Content-Type', 'application/json'
            );
            request.addListener('success', function(evt) {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'settings',
                        name: 'settings',
                        icon: 'everydesk/icons/16x16/cog.png'
                    }
                ));
                me.load();
                console.log(evt);
            });
            request.addListener('fail', function() {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'settings',
                        name: 'settings',
                        icon: 'everydesk/icons/16x16/cog.png'
                    }
                ));
            });
            request.setRequestData(content);
            request.send();
        },

        deleteSettings: function(content) {
            var me = this;
            qx.event.message.Bus.dispatch(new qx.event.message.Message(
                'actionstart', {
                    type: 'settings',
                    name: 'settings',
                    icon: 'everydesk/icons/16x16/cog.png'
                }
            ));
            var request = new everydesk.base.io.Xhr('/settings/' + content.id, 'DELETE');
            request.setRequestHeader(
                'X-EVERYDESK-SESSION',
                everydesk.base.system.Session.SESSIONKEY);
            request.addListener('success', function(evt) {
                console.log(evt);
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'settings',
                        name: 'settings',
                        icon: 'everydesk/icons/16x16/cog.png'
                    }
                ));
                me.load();
            });
            request.addListener('fail', function() {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'settings',
                        name: 'settings',
                        icon: 'everydesk/icons/16x16/cog.png'
                    }
                ));
            });
            request.send();
        }
    },

    /*
    ***************************************************************************
     EVENTS
    ***************************************************************************
    */
    events: {
        /**
         * Fired when the login was successful.
         */
        loadsuccess: 'qx.event.type.Data',
        /**
         * Fired when the login was not successful.
         */
        loadfailure: 'qx.event.type.Event'
    },

    construct: function() {
        this.base(arguments);
        var me = this;
        console.log("add event listener");
    }
});


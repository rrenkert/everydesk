/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.base.system.File', {
    extend: qx.core.Object,

    /*
    ***************************************************************************
     PROPERTIES
    ***************************************************************************
    */
    properties: {
        name: {check: 'String'},
        displayName: {check: 'String'},
        path: {check: 'String'},
        created: {check: 'Date', nullable: true},
        modified: {check: 'Date', nullable: true},
        size: {check: 'Number', nullable: true},
        type: {check: 'String'},
        mimeType: {check: 'String'},
        version: {check: 'String', nullable: true},
        children: {check: 'Array'}
    },

    events: {
    },

    construct: function(file) {
        var me = this;
        me.base(arguments);
        me.setName(file.name);
        me.setDisplayName(file.displayName);
        me.setPath(file.path);
        me.setCreated(file.created);
        me.setModified(file.modified);
        me.setSize(file.type === 'folder' ? 0 : file.size);
        me.setType(file.type);
        me.setMimeType(file.mimeType);
        me.setVersion(file.version === undefined ? '' : file.version);
        me.setChildren(file.children);
    }
});

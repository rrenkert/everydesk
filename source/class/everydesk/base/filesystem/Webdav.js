/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 */
qx.Class.define('everydesk.base.filesystem.Webdav', {
    extend: qx.core.Object,

    properties: {
        url: {
            check: 'String',
            nullable: false
        },
        sourcePath: {
            check: 'String',
            nullable: false
        },
        fileSystemPath: {
            check: 'String',
            nullable: false
        },
        basePath: {
            check: 'String',
            nullable: false
        }
    },
    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        ls: function(path, callback, scope) {
            var payload = '<?xml version="1.0" encoding="utf-8" ?><propfind xmlns="DAV:"><allprop/></propfind>' 
            this.__request(path, 'PROPFIND', payload, callback, scope);
        },

        get: function(file, callback, scope) {
            var requestUri = file.getPath();

            var request = new qx.bom.request.Xhr();
            request.onload = function() {
                callback.call(scope, true, file, request.getRequest().response);
            };
            request.open('GET', requestUri);
            request.setRequestHeader('X-EVERYDESK-SESSION', everydesk.base.system.Session.SESSIONKEY);
            request.getRequest().responseType = 'arraybuffer';
            request.send();
        },

        mkdir: function(parent, name, callback, scope) {
            var requestUri = parent.getPath() + '/' + name;
            this.__request(requestUri, 'MKCOL', undefined, function(success) {
                callback.call(scope, success, parent);
            });
        },

        save: function(parent, name, content, callback, scope) {
            var requestUri = parent.getPath() + '/' + name;
            this.__request(requestUri, 'PUT', content, function(success) {
                callback.call(scope, success, parent, name);
            });
        },

        cp: function(source, destination, callback, scope) {
            var request = new XMLHttpRequest();
            if (callback) {
                request.onreadystatechange = function() {
                    if (request.readyState === 4 &&
                        request.status === 201) {
                        callback.call(scope, true, destination);
                    }
                };
            }
            var url = source.getPath();
            request.open('COPY', url);
            request.setRequestHeader('Content-Type', 'text/xml; charset=UTF-8');
            request.setRequestHeader('Destination', this.getSourcePath() + destination);
            request.setRequestHeader('Overwrite', 'F');
            request.setRequestHeader('X-EVERYDESK-SESSION', everydesk.base.system.Session.SESSIONKEY);
            request.send();
        },

        rm: function(file, callback, scope) {
            var requestUri = file.getPath();
            this.__request(requestUri, 'DELETE', undefined, function(success) {
                callback.call(scope, success, file);
            });
        },

        mv: function(oldFile, newFile, callback, scope) {
            var me = this;

            var request = new XMLHttpRequest();
            if (callback) {
                request.onreadystatechange = function() {
                    if (request.readyState === 4 &&
                        request.status === 201) {
                        oldFile.setName(newFile);
                        oldFile.setPath(oldFile.parent.path + newFile);
                        oldFile.setDisplayName(newFile);
                        callback.call(scope, true, oldFile);
                    }
                };
            }
            var url = oldFile.getPath();
            request.open('MOVE', url);
            request.setRequestHeader('Content-Type', 'text/xml; charset=UTF-8');
            request.setRequestHeader('Destination', me.getSourcePath() + newFile);
            request.setRequestHeader('Overwrite', 'F');
            request.setRequestHeader('X-EVERYDESK-SESSION', everydesk.base.system.Session.SESSIONKEY);
            request.send();
        },

        __request: function(path, verb, payload, callback, scope) {
            var me = this;
            var request = new XMLHttpRequest();
            if (callback) {
                request.onreadystatechange = function() {
                    if (request.readyState === 4) {
                        if (verb === 'PROPFIND') {
                            me.__parsePropfind(request.responseXML, callback, scope);
                            return;
                        }
                        if ((request.status === 204 || request.status === 201)) {
                            callback.call(me, true);
                        }
                        else {
                            callback.call(me, false);
                        }
                    }
                };
            }
            var url = this.getUrl() + path;
            request.open(verb, url);
            request.setRequestHeader('Content-Type', 'text/xml; charset=UTF-8');
            request.setRequestHeader('X-EVERYDESK-SESSION', everydesk.base.system.Session.SESSIONKEY);
            request.send(payload);
        },

        __parsePropfind: function(response, callback, scope) {
            if (response.childNodes === null) {
                console.log('No such directory');
            }
            var responses = response.getElementsByTagNameNS('DAV:', 'response');
            var parentItem = responses[0];
            var parent = parentItem.getElementsByTagNameNS('DAV:', 'href')[0].firstChild.nodeValue.replace(this.getSourcePath(), this.getFileSystemPath() + '/' + this.getBasePath());
            var items = [];
            for (var i = 1; i < responses.length; i++) {
                var part = responses[i];
                var href = part.getElementsByTagNameNS('DAV:', 'href')[0].firstChild.nodeValue;
                var propstat = part.getElementsByTagNameNS('DAV:', 'propstat')[0];
                var prop = propstat.getElementsByTagNameNS('DAV:', 'prop')[0];
                var properties = this.__extractProperties(prop);
                var fileProperties = {};
                fileProperties.version = properties.version;
                fileProperties.quotaAvailable = properties.quotaAvailable;
                fileProperties.quotaUsed = properties.quotaUsed;
                fileProperties.getcontenttype = properties.getcontenttype;
                items.push(new everydesk.base.system.File({
                    name: this.__trimPath(href),
                    displayName: this.__trimPath(href),
                    path: href.replace(this.getSourcePath(), this.getFileSystemPath() + '/' + this.getBasePath()),
                    created: properties.lastModified,
                    modified: properties.lastModified,
                    size: properties.size,
                    type: properties.type,
                    mimeType: properties.mimeType,
                    sourcePath: this.getSourcePath(),
                    sourceType: 'webdav',
                    children: [],
                    properties: fileProperties
                }));
            }
            callback.call(scope, parent, items, this.getBasePath());
        },

        __extractProperties: function(node) {
            var properties = {};
            for (var i = 0; i < node.childNodes.length; i++) {
                var property = node.childNodes[i];
                if (property.namespaceURI !== 'DAV:') {
                    console.log('not DAV: namespace');
                    continue;
                }
                if (property.localName === 'getetag') {
                    properties.version = property.textContent;
                }
                if (property.localName === 'getlastmodified') {
                    properties.lastModified = new Date(property.textContent);
                }
                if (property.localName === 'quota-available-bytes') {
                    properties.quotaAvailable = Number(property.textContent);
                }
                if (property.localName === 'quota-used-bytes') {
                    properties.quotaUsed = Number(property.textContent);
                }
                if (property.localName === 'resourcetype' &&
                    property.childNodes[0] &&
                    property.childNodes[0].localName === 'collection') {
                    properties.type = 'folder';
                    properties.mimeType = 'folder';
                    properties.resourcetype = 1;
                }
                if (property.localName === 'getcontentlength') {
                    properties.size = Number(property.textContent);
                }
                if (property.localName === 'getcontenttype') {
                    properties.type = 'file';
                    properties.mimeType = property.textContent;
                }
            }
            if (properties.resourcetype === 1) {
                properties.getcontenttype = 'collection';
            }
            return properties;
        },

        __trimPath: function(content) {
            // remove trailing '/'
            var path = content.replace(/\/$/, '');
            if (path.lastIndexOf('/') === path.length - 1) {
                path = path.substring(0, path.length - 1);
            }
            if (path.lastIndexOf('/') < path.length - 1) {
                path = path.substring(
                    path.lastIndexOf('/') + 1,
                    path.length);
            }
            return decodeURIComponent(path);
        }
    },

    construct: function() {
    }
});

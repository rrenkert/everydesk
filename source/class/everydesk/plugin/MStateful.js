/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.plugin.MStateful', {
    extend: qx.core.Object,
    type: 'mixin',

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        __content: null,

        __uiState: null,

        __contentState: null,

        getState: function() {
            var me = this;
            me.__uiState = {
                width: me.getWidth(),
                height: me.getHeight(),
                layoutProperties: me.getLayoutProperties()
            };
            if (typeof me.__content === 'function') {
                me.contentState = me.__content();
            }
            return {
                ui: me.__uiState,
                content: me.__contentState
            };
        },

        setState: function(state) {
            var me = this;
            me.uiState = state.ui || null;
            me.contentState = state.content || null;
        },

        setContentState: function(content) {
            var me = this;
            me.__contentState = content;
        },

        setContent: function(content) {
            var me = this;
            me.__content = content;
        },

        saveState: function() {
            var me = this;
            var state = me.getState();

            var request = new everydesk.base.io.Xhr('/state', 'POST');
            request.setRequestHeader(
                'X-EVERYDESK-SESSION',
                everydesk.base.system.Session.SESSIONKEY);
            request.setRequestData(state);
            request.addListener('success', function(evt) {
                me.apps = evt.getTarget().getResponse();
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'statesaved',
                    state));
            });
            request.send();
        }
    },

    construct: function() {
        var me = this;
        me.base(arguments);
    }
});

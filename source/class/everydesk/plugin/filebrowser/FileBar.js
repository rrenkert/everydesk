/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.plugin.filebrowser.FileBar', {
    extend: everydesk.base.widget.OverflowToolbar,

    /*
    ***************************************************************************
     PROPERTIES
    ***************************************************************************
    */
    properties: {
    },

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        /**
         * @Override
         */
        add: function(child, options) {
            if (child === this.__overFlow) {
                this.base(arguments, child, options);
            }
            else {
                this.addBefore(child, this.__overFlow, options);
            }
        },

        clearOverflow: function() {
            this.getOverflowMenu().removeAll();
        },

        /**
         * @Override
         */
        remove: function(child) {
            var me = this;
            var children = me.getOverflowMenu().getChildren();
            for (var i = 0; i < children.length; i++) {
                if (children[i].getUserData('reference') === child) {
                    me.getOverflowMenu().remove(children[i]);
                    this.base(arguments, child);
                    return;
                }
            }
            this.base(arguments, child);
        },

        _showHideHandler: function(item, visibility) {
            var menuItem = null;
            if (item.classname === 'qx.ui.toolbar.Button') {
                menuItem = new qx.ui.menu.Button(item.getLabel(), item.getIcon());
                menuItem.addListener('execute', function(evt) {
                    item.execute(evt);
                });
            }
            else if (item.classname === 'qx.ui.toolbar.RadioButton') {
                menuItem = new qx.ui.menu.RadioButton(item.getLabel(), item.getIcon());
                menuItem.setGroup(item.getGroup());
                menuItem.addListener('execute', function(evt) {
                    item.execute(evt);
                });
            }
            else if (item.classname === 'qx.ui.toolbar.MenuButton') {
                menuItem = new qx.ui.menu.Button(item.getLabel(), item.getIcon(), null, item.getMenu());
            }
            else if (item.classname === 'qx.ui.toolbar.CheckBox') {
                menuItem = new qx.ui.menu.CheckBox(item.getLabel());
                menuItem.addListener('execute', function(evt) {item.execute(evt)});
            }
            else if (item.classname === 'qx.ui.toolbar.Part') {

            }
            else if (item.classname === 'qx.ui.toolbar.SplitButton') {

            }
            menuItem.setUserData('reference', item);
            this._addToMenu(menuItem, visibility);
        },

        _addToMenu: function(menuItem, visibility) {
            if (menuItem !== null && visibility === 'hide') {
                var existing = this.getOverflowMenu().getChildren()[0];
                if (existing) {
                    this.getOverflowMenu().addBefore(menuItem, existing);
                }
                else {
                    this.getOverflowMenu().add(menuItem);
                }
            }
            else {
                var children = this.getOverflowMenu().getChildren();
                for (var i = 0; i < children.length; i++) {
                    if (children[i].getUserData('reference') === menuItem.getUserData('reference')) {
                        this.getOverflowMenu().remove(children[i]);
                    }
                }
            }
        }
    }
});

/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.plugin.filebrowser.Settings', {
    extend: qx.core.Object,

    members: {
        __services: null,
        __form: null,
        __formModel: null,
        __controller: null,
        __currentService: null,

        getUI: function() {
            var me = this;
            var container = new qx.ui.container.Composite();
            this.__services = everydesk.base.system.FileManager.getInstance().getServices();
            me.__tableModel = new qx.ui.table.model.Simple();
            me.__tableModel.setColumns(['Name', 'Type', '']);
            var colModel = {
                tableColumnModel: function(obj) {
                    return new qx.ui.table.columnmodel.Resize(obj);
                }
            };
            var table = new qx.ui.table.Table(me.__tableModel, colModel);
            var layout = new qx.ui.layout.VBox();
            container.setLayout(layout);
            container.add(table, {flex: 1});

            table.setStatusBarVisible(false);
            table.setShowCellFocusIndicator(false);
            table.setFocusCellOnPointerMove(true);
            table.setColumnVisibilityButtonVisible(false);
            table.getDataRowRenderer().setHighlightFocusRow(true);
            table.getSelectionModel().setSelectionMode(2);

            table.addListener('cellTap', function(evt) {
                var row = evt.getRow();
                var col = evt.getColumn();
                console.log(row + ' - ' + col);
                var id = me.__tableModel.getValue(3, row);
                var service;
                for (var j = 0; j < this.__services.length; j++) {
                    if (id === this.__services[j].id) {
                        service = this.__services[j];
                        break;
                    }
                }
                if (col === 2) {
                    // TODO Delete fileservice.
                    return;
                }
                container.removeAt(2);
                container.add(this.__buildForm(service, {flex: 2}));
                this.__updateForm(service);
            }, me);

            var tcm = table.getTableColumnModel();
            tcm.getBehavior().setWidth(0, '1*');
            tcm.getBehavior().setWidth(1, '1*');
            tcm.getBehavior().setWidth(2, 26);
            var renderer = new qx.ui.table.cellrenderer.Image(16, 16);
            renderer.setRepeat('scale');
            tcm.setDataCellRenderer(2, renderer);
            var rows = [];
            for (var i = 0; i < this.__services.length; i++) {
                rows.push([
                    this.__services[i].name,
                    this.__services[i].type.name,
                    'everydesk/icons/16x16/delete.png',
                    this.__services[i].id
                ]);
            }
            me.__tableModel.setData(rows);
            var tools = new qx.ui.toolbar.ToolBar();
            var newMenuButton = new qx.ui.toolbar.MenuButton('New');
            var newMenu = new qx.ui.menu.Menu();
            var newWebdav = new qx.ui.menu.Button('Webdav');
            newWebdav.addListener('execute', function() {
                container.removeAt(2);
                me.__currentService = {};
                me.__currentService.id = undefined;
                me.__currentService.type = {name: 'webdav', id: 1};
                me.__currentService.name = '';
                me.__currentService.user = '';
                me.__currentService.url = '';
                me.__currentService.path = 'fs-' + me.__services.length + 1;
                container.add(me.__buildForm(me.__currentService), {flex: 2});
                me.__controller.resetModel();
                me.__formModel = me.__controller.createModel();
                me.__updateForm(me.__currentService);
            });
            var newDropbox = new qx.ui.menu.Button('Dropbox');
            newDropbox.addListener('execute', function() {
                container.removeAt(2);
                me.__currentService = {};
                me.__currentService.id = undefined;
                me.__currentService.type = {name: 'dropbox', id: 2};
                me.__currentService.name = '';
                me.__currentService.path = 'fs-' + me.__services.length + 1;
                container.add(me.__buildForm(me.__currentService), {flex: 2});
                me.__controller.resetModel();
                me.__formModel = me.__controller.createModel();
                me.__updateForm(me.__currentService);
            });
            newMenu.add(newWebdav);
            newMenu.add(newDropbox);
            newMenuButton.setMenu(newMenu);
            tools.addSpacer();
            tools.add(newMenuButton);
            container.add(tools);
            container.add(this.__buildForm(this.__services[0]), {flex: 2});
            return container;
        },

        __buildForm: function(service) {
            var me = this;
            var groupBox = new qx.ui.groupbox.GroupBox('Service');
            groupBox.setLayout(new qx.ui.layout.Canvas());
            var saveButton = new qx.ui.form.Button('Save');
            var dbButton = new qx.ui.form.Button('Request Access-Key');
            this.__form = new qx.ui.form.Form();
            var type = new qx.ui.form.TextField();
            type.setReadOnly(true);
            this.__form.add(type, 'Type', null, 'type');
            this.__form.add(new qx.ui.form.TextField(), 'Name', null, 'name');
            if (service && service.type.name === 'webdav') {
                this.__form.add(new qx.ui.form.TextField(), 'Url', null, 'url');
                this.__form.add(new qx.ui.form.TextField(), 'User', null, 'user');
            }
            this.__form.add(new qx.ui.form.PasswordField(), 'Password', null, 'password');
            if (service && service.type.name === 'dropbox') {
                this.__form.addButton(dbButton);
            }
            this.__form.addButton(saveButton);
            this.__controller = new qx.data.controller.Form(null, this.__form);
            this.__formModel = this.__controller.createModel();
            dbButton.addListener('execute', function(evt) {
                var request = new everydesk.base.io.Xhr('/dbox/oauth', 'GET');
                request.setRequestHeader(
                    'X-EVERYDESK-SESSION',
                    everydesk.base.system.Session.SESSIONKEY);
                request.setRequestHeader(
                    'Content-Type', 'application/json'
                );
                request.addListener('success', function(evt1) {
                    console.log(evt1.getTarget().getResponse());
                    var win = window.open(evt1.getTarget().getResponse(), '_blank');
                    if (!win) {
                        // TODO: show message to allow popups
                        return;
                    }
                    win.focus();
                });
                request.addListener('fail', function() {
                });
                request.send();
            });
            saveButton.addListener('execute', function() {
                me.__currentService.name = me.__formModel.getName();
                me.__currentService.password = me.__formModel.getPassword();
                var type = me.__formModel.getType();
                me.__currentService.type = {};
                me.__currentService.type.name = type;
                if (type === 'webdav') {
                    me.__currentService.url = me.__formModel.getUrl();
                    me.__currentService.login = me.__formModel.getUser();
                    me.__currentService.password = me.__formModel.getPassword();
                    var tmpSub = me.__formModel.getUrl().split('://')[1];
                    var sourcePath = tmpSub.substring(tmpSub.indexOf('/'), tmpSub.length);
                    me.__currentService.sourcePath = sourcePath;
                }
                else {
                    me.__currentService.url = '';
                    me.__currentService.login = '';
                    me.__currentService.password = me.__formModel.getPassword();
                    me.__currentService.sourcePath = '';
                }
                me.__saveFileservice(me.__currentService);
            });
            groupBox.add(new qx.ui.form.renderer.Single(this.__form));
            return groupBox;
        },

        __saveFileservice: function(service) {
            var request = new everydesk.base.io.Xhr('/fileservices', 'POST');
            request.setRequestHeader(
                'X-EVERYDESK-SESSION',
                everydesk.base.system.Session.SESSIONKEY);
            request.setRequestHeader(
                'Content-Type', 'application/json'
            );
            request.addListener('success', function(evt) {
                console.log(evt);
            });
            request.addListener('fail', function() {
            });
            request.setRequestData(service);
            request.send();
        },

        __updateForm: function(service) {
            this.__currentService = service;
            this.__formModel.setName(service.name);
            this.__formModel.setType(service.type.name);
            if (service.type.name === 'webdav') {
                this.__formModel.setUrl(service.url);
                this.__formModel.setUser(service.user);
            }
        }
    },

    construct: function() {
        this.base(arguments);
    }
});


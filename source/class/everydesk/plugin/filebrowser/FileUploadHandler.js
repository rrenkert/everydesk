/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

qx.Class.define('everydesk.plugin.filebrowser.FileUploadHandler', {
    extend: com.zenesis.qx.upload.AbstractHandler,

    members: {
        __uploadFolder: null,
        __uploading: false,

        /*
         * @Override
         */
        addBlob: function(filename, blob, params) {
            var id = 'upload-' + this._getUniqueFileId();
            var file = new com.zenesis.qx.upload.File(blob, filename, id);
            if (params) {
                for (var name in params) {
                    if (!name) {
                        continue;
                    }
                    var value = params[name];
                    if (value !== null) {
                        file.setParam(name, value);
                    }
                }
            }
            this._addFile(file);
        },

        /*
        * @Override
        */
        _createFile: function(input) {
            var bomFiles = input.files;
            if (!bomFiles || !bomFiles.length) {
                this.debug('No files found to upload via FileUploadHandler');
            }
            var files = [];
            for (var i = 0; i < bomFiles.length; i++) {
                var bomFile = bomFiles[i];
                var id = 'upload-' + this._getUniqueFileId();
                var filename = typeof bomFile.name !== 'undefined' ? bomFile.name : bomFile.fileName;
                var file = new com.zenesis.qx.upload.File(bomFile, filename, id);
                var fileSize = typeof bomFile.size !== 'undefined' ? bomFile.size : bomFile.fileSize;
                file.setSize(fileSize);
                files.push(file);
            }

            return files;
        },

        /*
         * @Override
         */
        _doUpload: function(file) {
            var me = this;
            if (!me.__uploading) {
                me.__uploading = true;
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'file:__savestarted', {
                        file: me.__uploadFolder
                    }
                ));
            }
            var parentFolder = me.__uploadFolder;
            var filemanager = everydesk.base.system.FileManager.getInstance();
            if (!file) {
                return;
            }
            var content;
            if (typeof file.getBrowserObject().getAsBinary === 'function') {
                content = file.getBrowserObject().getAsBinary();
                filemanager.save(parentFolder, file.getFilename(), content);
            }
            else {
                var reader = new FileReader();
                reader.onload = function(evt) {
                    content = evt.target.result;
                    filemanager.save(parentFolder, file.getFilename(), content);
                };
                reader.readAsArrayBuffer(file.getBrowserObject());
            }
        },

        /*
        *@Override
        */
        _onCompleted: function(evt) {
            var me = this;
            // var file = evt.getData().file;
            var success = evt.getData().success;
            var name = evt.getData().name;
            var current = this.__current;
            for (var i = 0; i < current.length; i++) {
                if (current[i].getFilename() === name) {
                    current.splice(i, 1);
                    break;
                }
            }
            if (current.length === 0 && this.__queue.length === 0) {
                me.__uploading = false;
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'file:__savecompleted', {
                        success: success,
                        file: me.__uploadFolder
                    }
                ));
                return;
            }
            // Start the next one
            this.beginUploads();
        },

        /*
        * @Override
        */
        _doCancel: function(file) {
            return file;
        }
    },

    construct: function(uploader) {
        this.base(arguments, uploader);
        qx.event.message.Bus.subscribe('file:__save', this._onCompleted, this);
    }
});

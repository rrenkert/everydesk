/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 */
qx.Class.define('everydesk.plugin.filebrowser.FileSave', {
    extend: everydesk.plugin.Dialog,

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        __list: null,
        __fileTree: null,
        __currentFolder: null,
        __filebar: null,
        __toolbarFilePart: null,
        __currentLayer: 0,
        __layers: [],

        init: function() {
            var me = this;
            me.base(arguments);
            me.__fileTree = everydesk.base.system.FileManager.getInstance().getFileSystem();
            me.__fileGroup = new qx.ui.form.RadioGroup();
            me.__fileGroup.setAllowEmptySelection(false);
            me.__refreshButton = new qx.ui.toolbar.Button(null, 'everydesk/icons/16x16/arrow_refresh.png');
            me.__newFolderButton = new qx.ui.toolbar.Button(null, 'everydesk/icons/16x16/folder_new.png');
            me.__refreshButton.setEnabled(false);
            me.__newFolderButton.setEnabled(false);
            me.__refreshButton.addListener('execute', function() {
                if (me.__currentLayer < 1) {
                    return;
                }
                var filemanager = everydesk.base.system.FileManager.getInstance();
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionstart', {
                        type: 'file',
                        icon: 'everydesk/icons/16x16/folder.png'
                    }
                ));
                filemanager.refresh(
                    me.__layers[me.__currentLayer].file,
                    me.__layers[1].file.getName()
                );
            });
            me.__newFolderButton.addListener('execute', function() {
                me.__list.addFolder(me.__layers[me.__currentLayer].file);
            });
            me.__filebar = new everydesk.plugin.filebrowser.FileBar();
            me.__filebar.add(me.__refreshButton);
            me.__filebar.add(me.__newFolderButton);
            me.__filebar.setOverflowHandling(true);
            me.add(me.__filebar);
            me.__fileNameBar = new qx.ui.toolbar.ToolBar();
            var fileLabel = new qx.ui.basic.Label('Name');
            fileLabel.setMargin(3, 3, 3, 10);
            fileLabel.setAlignY('middle');
            var fileNameField = new qx.ui.form.TextField();
            fileNameField.setMargin(3, 3, 3, 7);
            me.__fileNameBar.add(fileLabel);
            me.__fileNameBar.add(fileNameField, {
                flex: 1
            });
            me.add(me.__fileNameBar);

            var mainLayout = new qx.ui.layout.VBox();
            me.setLayout(mainLayout);
            me.__list = new everydesk.plugin.filebrowser.FileList();
            me.__list.getList().addListener('dblclick', function(evt) {
                evt.stopPropagation();
                var selectionModel = evt.getCurrentTarget().getSelectionModel();
                if (selectionModel.getSelectedCount() > 0) {
                    var ndx = selectionModel.getSelectedRanges()[0].maxIndex;
                    var file = me.__list.getModel().getRowData(ndx)[3];
                    if (file && file.getType() === 'file') {
                        var testMsg = new everydesk.base.widget.Message({
                            title: 'File exists. Overwrite?',
                            text: 'The selected File already exists. Do you want overwrite?',
                            buttons: everydesk.base.widget.Message.Buttons.YES_NO,
                            type: everydesk.base.widget.Message.Type.ALERT,
                            callback: function(value) {
                                if (value === -1) {
                                    return;
                                }
                                me.__layers.splice(0, me.__layers.length);
                                me.__currentLayer = 0;
                                if (me.__callback) {
                                    me.__callback.call(me.__referer, file);
                                }
                                // me.__referer.open(file);
                                me.close();
                            }
                        });
                        testMsg.show();
                    }
                    else if (file && file.getType() === 'folder') {
                        me.__list.setFiles(file.getChildren());
                        me.__list.getList().resetSelection();
                        me.__filterbar.applyFilter();
                        me.__addFolderButton(file);
                        if (me.__currentLayer < 1) {
                            me.__refreshButton.setEnabled(false);
                            me.__newFolderButton.setEnabled(false);
                            me.__saveButton.setEnabled(false);
                        }
                        else {
                            me.__refreshButton.setEnabled(true);
                            me.__newFolderButton.setEnabled(true);
                            me.__saveButton.setEnabled(true);
                        }
                    }
                }
            }, me, true);
            me.add(me.__list, {
                flex: 1
            });

            me.__filterbar = new everydesk.plugin.filebrowser.FilterBar();
            me.__filterbar.setModel(me.__list.getModel());
            me.__filterbar.addSpacer();
            var cancelButton = new qx.ui.toolbar.Button('Cancel', 'everydesk/icons/16x16/cancel.png');
            cancelButton.setMargin(3, 3, 3, 3);
            cancelButton.addListener('execute', function() {
                me.close();
            });
            me.__saveButton = new qx.ui.toolbar.Button('Save', 'everydesk/icons/16x16/save_as.png');
            me.__saveButton.setMargin(3, 3, 3, 3);
            me.__saveButton.setEnabled(false);
            me.__saveButton.addListener('execute', function() {
                if (me.__currentLayer < 1 ||
                    fileNameField.getValue() === null) {
                    return;
                }
                var parent = me.__layers[me.__currentLayer].file;
                var name = fileNameField.getValue();
                if (!name.endsWith(me.__filterbar.getCurrentFilter()[2])) {
                    name += me.__filterbar.getCurrentFilter()[2];
                }
                
                var filemanager = everydesk.base.system.FileManager.getInstance();
                var exists = filemanager.getFile(parent.getPath() + name);
                if (exists) {
                    var testMsg = new everydesk.base.widget.Message({
                        title: 'File exists. Overwrite?',
                        text: 'The selected File already exists. Do you want overwrite?',
                        buttons: everydesk.base.widget.Message.Buttons.YES_NO,
                        type: everydesk.base.widget.Message.Type.ALERT,
                        callback: function(value) {
                            if (value === -1) {
                                return;
                            }
                            me.__layers.splice(0, me.__layers.length);
                            me.__currentLayer = 0;
                            if (me.__callback) {
                                me.__callback.call(me.__referer, exists);
                            }
                            // me.__referer.open(file);
                            me.close();
                        }
                    });
                    testMsg.show();
                }
                else {
                    var file = new everydesk.base.system.File({
                        name: name,
                        displayName: name,
                        path: parent.getPath() + name,
                        created: new Date(),
                        modified: new Date(),
                        size: 0,
                        type: 'file',
                        mimeType: '',
                        version: '',
                        children: []
                    });
                    file.parent = parent;
                    if (me.__callback) {
                        me.__callback.call(me.__referer, file);
                    }
                    me.close();
                }
            });
            me.__filterbar.add(cancelButton);
            me.__filterbar.add(me.__saveButton);
            me.add(me.__filterbar);
            me.open('/fs/');
            me.__buildFilebar();
        },

        __buildFilebar: function() {
            var me = this;
            var root = new qx.ui.toolbar.RadioButton(me.__fileTree.name);
            root.setUserData('ndx', 0);
            root.addListener('execute', me.__open, me);
            me.__layers = [{btn: root, file: me.__fileTree}];
            me.__filebar.add(root);
            me.__fileGroup.add(root);
            me.__currentLayer = 0;
        },

        __addFolderButton: function(file) {
            var me = this;
            me.__filebar.setOverflowHandling(false);
            me.__filebar.clearOverflow();
            me.__currentLayer++;
            if (me.__currentLayer < me.__layers.length) {
                for (var i = me.__currentLayer; i < me.__layers.length; i++) {
                    me.__filebar.remove(me.__layers[i].btn);
                }
                me.__layers.splice(me.__currentLayer, me.__layers.length);
            }
            var button = new qx.ui.toolbar.RadioButton(file.getDisplayName());
            button.setUserData('ndx', me.__currentLayer);
            button.addListener('execute', me.__open, me);
            me.__fileGroup.add(button);
            me.__layers.push({btn: button, file: file});
            me.__filebar.add(button);
            button.toggleValue();
            me.__filebar.setOverflowHandling(true);
        },

        __open: function(evt) {
            var me = this;
            var ndx = evt.getCurrentTarget().getUserData('ndx');
            me.__currentLayer = ndx;
            var select = me.__layers[ndx];
            if (me.__currentLayer < 1) {
                me.__refreshButton.setEnabled(false);
                me.__newFolderButton.setEnabled(false);
                me.__saveButton.setEnabled(false);
            }
            else {
                me.__refreshButton.setEnabled(true);
                me.__newFolderButton.setEnabled(true);
                me.__saveButton.setEnabled(true);
            }
            if (select.file.path) {
                me.open(select.file.path);
            }
            else {
                me.open(select.file.getPath());
            }
        },

        setReferer: function(referer) {
            this.__referer = referer;
            this.__filterbar.setFileTypes(referer.getFileTypes());
        },

        open: function(path) {
            var me = this;
            var file = me.__getFileNode(path, me.__fileTree);
            if (file.children) {
                me.__list.setFiles(file.children);
            }
            else {
                me.__list.setFiles(file.getChildren());
            }
            me.__filterbar.applyFilter();
        },

        __getFileNode: function(path, node) {
            var parts = path.split('/');
            if (path === '/fs/') {
                return node;
            }

            var children = node.children;
            var ndx = 2;

            for (var i = 0; i < children.length; i++) {
                var nodeParts = children[i].getPath().split('/');
                if (parts[ndx] === nodeParts[ndx] && ndx === parts.length - 2) {
                    return children[i];
                }
                else if (parts[ndx] === nodeParts[ndx]) {
                    var current = children[i];
                    while (current && ndx < parts.length) {
                        ndx++;
                        var currChildren = current.getChildren();
                        for (var j = 0; j < currChildren.length; j++) {
                            var childParts = currChildren[j].getPath().split('/');
                            if (childParts[ndx] === parts[ndx] &&
                                ndx === parts.length - 2) {
                                return currChildren[j];
                            }
                            else if (childParts[ndx] === parts[ndx]) {
                                current = currChildren[j];
                                break;
                            }
                        }
                    }
                }
            }
        }
    },

    construct: function() {
        var me = this;
        me.base(arguments);
        me.setWidth(400);
        me.setHeight(300);

        me.init();
    }
});


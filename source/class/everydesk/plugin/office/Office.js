/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.plugin.office.Office', {
    extend: everydesk.plugin.Application,

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        __fileTypesDoc: [
            ['MSOffice Format (.docx)', '.*.docx$', '.docx'],
            ['Open Document Format (.odt)', '.*.odt$', 'odt']
        ],
        __fileTypesSpread: [
            ['MSOffice Format (.xlsx)', '.*.xlsx$', '.xlsx'],
            ['Open Document Format (.ods)', '.*.ods$', 'ods']
        ],
        __fileTypesPres: [
            ['MSOffice Format (.pptx)', '.*.pptx$', '.pptx'],
            ['Open Document Format (.otp)', '.*.odp$', 'odp']
        ],

        __file: null,
        __content: null,
        __blob: null,

        init: function() {
            var me = this;
            me.setCaption('Office');
            me.setContentPadding(0);

            var toolbar = new qx.ui.toolbar.ToolBar();
            toolbar.setSpacing(3);
            me.add(toolbar);
            var tbPart1 = new qx.ui.toolbar.Part();
            var menudoc = new qx.ui.menu.Menu();
            var newDocButton = new qx.ui.menu.Button('New...');
            var openDocButton = new qx.ui.menu.Button('Open...');
            menudoc.add(newDocButton);
            menudoc.add(openDocButton);
            me.__docButton = new qx.ui.toolbar.MenuButton(
                'Document',
                'everydesk/icons/16x16/page_white_word.png',
                menudoc
            );
            newDocButton.addListener('execute', me.newDocument, me);
            openDocButton.addListener('execute', me.openDocument, me);
            var menuspread = new qx.ui.menu.Menu();
            var newSpreadButton = new qx.ui.menu.Button('New...');
            var openSpreadButton = new qx.ui.menu.Button('Open...');
            menuspread.add(newSpreadButton);
            menuspread.add(openSpreadButton);
            me.__spreadButton = new qx.ui.toolbar.MenuButton(
                'New Spreadsheet',
                'everydesk/icons/16x16/page_white_excel.png',
                menuspread
            );
            newSpreadButton.addListener('execute', me.newSpreadsheet, me);
            openSpreadButton.addListener('execute', me.openSpreadsheet, me);
            var menupres = new qx.ui.menu.Menu();
            var newPresButton = new qx.ui.menu.Button('New...');
            var openPresButton = new qx.ui.menu.Button('Open...');
            menupres.add(newPresButton);
            menupres.add(openPresButton);
            me.__presButton = new qx.ui.toolbar.MenuButton(
                'New presentation',
                'everydesk/icons/16x16/page_white_powerpoint.png',
                menupres
            );
            newPresButton.addListener('execute', me.newPresentation, me);
            openPresButton.addListener('execute', me.openPresentation, me);
            me.__saveAsButton = new qx.ui.toolbar.Button(
                'Save as...',
                'everydesk/icons/16x16/save_as.png'
            );
            me.__saveAsButton.addListener('execute', me.saveAsNewFile, me);
            tbPart1.add(me.__docButton);
            tbPart1.add(me.__spreadButton);
            tbPart1.add(me.__presButton);
            tbPart1.add(me.__saveAsButton);
            tbPart1.setShow('icon');
            toolbar.add(tbPart1);

            var mainContainer = new qx.ui.container.Composite();
            mainContainer.setPadding(0, 0, 0, 0);
            var contLayout = new qx.ui.layout.VBox();
            mainContainer.setLayout(contLayout);

            me.__content = new qx.ui.embed.ThemedIframe();
            me.__content.setPadding(0, 0, 0, 0);
            me.__content.addListener('load', function(iframe) {
                if (me.__file) {
                    var doc = iframe.getTarget().getBody().ownerDocument.defaultView;
                    doc.blob = new Blob([new Uint8Array(me.__blob)]);
                    doc.edAuth = everydesk.base.system.Session.SESSIONKEY;
                    doc.file = window.location.origin + me.__file.getPath() + '?EVERYDESK-SESSION=';
                    doc.fileName = me.__file.getDisplayName();
                    doc.key = me.__file.getVersion().substring(1, 20);
                    doc.fileType = me.__file.getName().split('.')[1];
                    doc.docType = me.__findDocType(me.__file.getName());
                    doc.modDate = me.__file.getModified;
                    doc.callbackUrl = window.location.origin + me.__createCallbackUrl(me.__file.getPath()) + '?EVERYDESK-SESSION=';
                    doc.author = everydesk.base.system.Session.USERNAME;
                    doc.focusCallback = function() {
                        me.setActive(true);
                    };
                    doc.connectEditor();
                }
            }, this);
            var layout = new qx.ui.layout.VBox();
            me.setLayout(layout);
            mainContainer.add(me.__content, {
                flex: 1
            });
            me.add(mainContainer, {
                flex: 1
            });
            me.addListener('close', me.__doClose, me);
            me.center();
        },

        __createCallbackUrl: function(path) {
            return path.replace('/fs', '/document/edit');
        },

        open: function(file) {
            var me = this;
            if (!file) {
                return;
            }
            me.__file = file;
            var filemanager = everydesk.base.system.FileManager.getInstance();
            me.__content.resetSource();
            filemanager.open(file, me.startUp, me);
            // me.__content.reload();
            // me.__content.setSource('resource/everydesk/office/office.html');
        },

        startUp: function(success, file, content) {
            if (!success) {
                this.__doClose();
            }
            var me = this;
            me.__blob = content;
            me.__content.setSource(qx.util.ResourceManager.getInstance().toUri('resource/everydesk/office/office.html'));
        },

        __doClose: function() {
            var me = this;
            me.destroy();
        },

        __findDocType: function(filename) {
            var docReg = new RegExp('.*\.(odt|doc|docx)$');
            var pptReg = new RegExp('.*\.(odp|ppt|pptx)$');
            var xlsReg = new RegExp('.*\.(ods|xls|xlsx)$');
            if (filename.match(docReg)) {
                return 'text';
            }
            else if (filename.match(pptReg)) {
                return 'presentation';
            }
            else if (filename.match(xlsReg)) {
                return 'spreadsheet';
            }
        },

        create: function(file) {
            var me = this;
            var url = file.getPath();
            url = url.replace('/fs', '/document/new');
            // Do the request.
            var request = new XMLHttpRequest();
            request.onreadystatechange = function() {
                if (request.readyState === 4) {
                    me.__file = file;
                    var filemanager = everydesk.base.system.FileManager.getInstance();
                    me.__content.resetSource();
                    filemanager.open(file, me.startUp, me);
                    var parts = file.getPath().split('/');
                    var fs = parts[2];
                    filemanager.refresh(file.parent, fs);
                }
            };
            request.open('POST', url);
            request.setRequestHeader('X-EVERYDESK-SESSION', everydesk.base.system.Session.SESSIONKEY);
            request.send('{"type":"' + me.__state + '"}');
        },

        save: function(file) {

        },

        saveAsNewFile: function() {
            var me = this;
            var msg = new qx.event.message.Message(
                'showdialog', {
                    endpoint: 'everydesk.plugin.filebrowser.FileSave',
                    icon: 'everydesk/icons/16x16/folder.png',
                    name: 'Save as...',
                    file: '/fs/',
                    callback: me.save
                }
            );
            msg.setSender(me);
            qx.event.message.Bus.dispatch(msg);
        },

        openDocument: function() {
            var me = this;
            me.__state = 'document';
            var msg = new qx.event.message.Message(
                'showdialog', {
                    endpoint: 'everydesk.plugin.filebrowser.FileOpen',
                    icon: 'everydesk/icons/16x16/folder.png',
                    name: 'Open Document',
                    file: '/fs/',
                    callback: me.open
                }
            );
            msg.setSender(me);
            qx.event.message.Bus.dispatch(msg);
        },

        newDocument: function() {
            var me = this;
            me.__state = 'document';
            var msg = new qx.event.message.Message(
                'showdialog', {
                    endpoint: 'everydesk.plugin.filebrowser.FileSave',
                    icon: 'everydesk/icons/16x16/folder.png',
                    name: 'New Document',
                    file: '/fs/',
                    callback: me.create
                }
            );
            msg.setSender(me);
            qx.event.message.Bus.dispatch(msg);
        },

        openSpreadsheet: function() {
            var me = this;
            me.__state = 'spreadsheet';
            var msg = new qx.event.message.Message(
                'showdialog', {
                    endpoint: 'everydesk.plugin.filebrowser.FileOpen',
                    icon: 'everydesk/icons/16x16/folder.png',
                    name: 'Open Spreadsheet',
                    file: '/fs/',
                    callback: me.open
                }
            );
            msg.setSender(me);
            qx.event.message.Bus.dispatch(msg);
        },

        newSpreadsheet: function() {
            var me = this;
            me.__state = 'spreadsheet';
            var msg = new qx.event.message.Message(
                'showdialog', {
                    endpoint: 'everydesk.plugin.filebrowser.FileSave',
                    icon: 'everydesk/icons/16x16/folder.png',
                    name: 'New Spreadsheet',
                    file: '/fs/',
                    callback: me.create
                }
            );
            msg.setSender(me);
            qx.event.message.Bus.dispatch(msg);
        },

        openPresentation: function() {
            var me = this;
            me.__state = 'presentation';
            var msg = new qx.event.message.Message(
                'showdialog', {
                    endpoint: 'everydesk.plugin.filebrowser.FileOpen',
                    icon: 'everydesk/icons/16x16/folder.png',
                    name: 'Open Presentation',
                    file: '/fs/',
                    callback: me.open
                }
            );
            msg.setSender(me);
            qx.event.message.Bus.dispatch(msg);
        },

        newPresentation: function() {
            var me = this;
            me.__state = 'presentation';
            var msg = new qx.event.message.Message(
                'showdialog', {
                    endpoint: 'everydesk.plugin.filebrowser.FileSave',
                    icon: 'everydesk/icons/16x16/folder.png',
                    name: 'New Presentation',
                    file: '/fs/',
                    callback: me.create
                }
            );
            msg.setSender(me);
            qx.event.message.Bus.dispatch(msg);
        },

        getFileTypes: function() {
            if (this.__state === 'document') {
                return this.__fileTypesDoc;
            }
            if (this.__state === 'spreadsheet') {
                return this.__fileTypesSpread;
            }
            if (this.__state === 'presentation') {
                return this.__fileTypesPres;
            }
            return this.__fileTypesDoc.concat(
                this.__fileTypesSpread
            ).concat(this.__fileTypesPres);
        }
    },

    construct: function() {
        var me = this;
        me.base(arguments);
        me.setWidth(1100);
        me.setHeight(700);
        me.init();
        me.centerWindow();
    }
});

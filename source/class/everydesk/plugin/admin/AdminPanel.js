/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 */
qx.Class.define('everydesk.plugin.admin.AdminPanel', {
    extend: everydesk.plugin.Application,

    members: {
        __userTableModel: null,
        __users: null,

        init: function() {
            var me = this;
            me.setCaption('AdminPanel');
            me.setContentPadding(5);
            var mainLayout = new qx.ui.layout.VBox();
            me.setLayout(mainLayout);
            var tabView = new qx.ui.tabview.TabView();
            var applications = new qx.ui.tabview.Page('Applications', 'everydesk/icons/16x16/application.png');
            applications.setLayout(new qx.ui.layout.VBox());
            applications.setPadding(0, 0, 0, 0);
            applications.add(me.__loadApps());
            var users = new qx.ui.tabview.Page('Users', 'everydesk/icons/16x16/users_3.png');
            users.setLayout(new qx.ui.layout.VBox());
            users.setPadding(0, 0, 0, 0);
            users.add(me.__loadUsers());
            tabView.add(applications);
            tabView.add(users);
            me.add(tabView);
        },

        __loadUsers: function() {
            var me = this;
            var container = new qx.ui.container.Composite();
            var layout = new qx.ui.layout.VBox();
            container.setLayout(layout);
            container.setPadding(0, 0, 0, 0);
            me.__userTableModel = new qx.ui.table.model.Simple();
            me.__userTableModel.setColumns(['Name', 'Forename', 'Surname', 'Active', 'Admin', 'external', '']);
            var colModel = {
                tableColumnModel: function(obj) {
                    return new qx.ui.table.columnmodel.Resize(obj);
                }
            };
            var table = new qx.ui.table.Table(this.__userTableModel, colModel);
            table.setStatusBarVisible(false);
            table.setShowCellFocusIndicator(false);
            table.setFocusCellOnPointerMove(true);
            table.setColumnVisibilityButtonVisible(false);
            table.getDataRowRenderer().setHighlightFocusRow(true);
            table.setMaxHeight(300);

            table.addListener('cellTap', function(evt) {
                var row = evt.getRow();
                var col = evt.getColumn();
                console.log(row + ' - ' + col);
                var user = me.__userTableModel.getValue(7, row);
                if (col === 6) {
                    console.log(user);
                    //everydesk.base.system.FavoritesManager.getInstance().deleteUser(user);
                    //me.__clearDetails();
                }
                else {
                    for (var i = 0; i < me.__users.length; i++) {
                        console.log(me.__users[i]);
                        if (me.__users[i].id === user) {
                            me.__fillUserDetails(me.__users[i]);
                        }
                    }
                    //me.__fillDetails();
                }
            }, me);

            var selModel = table.getSelectionModel();
            selModel.setSelectionMode(2);

            selModel.addListener('changeSelection', function(evt) {
                var selectionModel = evt.getCurrentTarget();
                if (selectionModel.getSelectedCount() === 1) {
                    var ndx = selectionModel.getSelectedRanges()[0].maxIndex;
                    var userId = me.__userTableModel.getValue(6, ndx);
                    console.log(userId);
                    for (var i = 0; i < me.__users.length; i++) {
                        console.log(me.__users[i]);
                        if (me.__users[i].id === userId) {
                            me.__fillUserDetails(me.__users[i]);
                        }
                    }
                }
                else {
                    return;
                }
            });
            var tcm = table.getTableColumnModel();
            tcm.getBehavior().setWidth(0, '1*');
            tcm.getBehavior().setWidth(1, 100);
            tcm.getBehavior().setWidth(2, 100);
            tcm.getBehavior().setWidth(3, 60);
            tcm.getBehavior().setWidth(4, 60);
            tcm.getBehavior().setWidth(5, 60);
            tcm.getBehavior().setWidth(6, 26);
            tcm.setDataCellRenderer(
                3,
                new qx.ui.table.cellrenderer.Boolean());
            tcm.setDataCellRenderer(
                4,
                new qx.ui.table.cellrenderer.Boolean());
            tcm.setDataCellRenderer(
                5,
                new qx.ui.table.cellrenderer.Boolean());
            var renderer = new qx.ui.table.cellrenderer.Image(16, 16);
            renderer.setRepeat('scale');
            tcm.setDataCellRenderer(6, renderer);
            container.add(table, {flex: 1, height: '40%'});

            me.__details = new qx.ui.groupbox.GroupBox('User');
            me.__details.setMargin(5, 0, 0, 0);
            var formLayout = new qx.ui.layout.Grid(0, 5);
            formLayout.setColumnWidth(0, 60);
            formLayout.setColumnWidth(1, 200);
            formLayout.setColumnWidth(2, 60);
            formLayout.setColumnWidth(3, 200);
            me.__details.setLayout(formLayout);


            var nameLabel = new qx.ui.basic.Label('Name');
            me.__detailsName = new qx.ui.form.TextField();
            me.__detailsName.setMarginRight(10);
            nameLabel.setAlignY('middle');
            me.__details.add(nameLabel, {row: 0, column: 0});
            me.__details.add(me.__detailsName, {row: 0, column: 1, colSpan: 1});

            var mailLabel = new qx.ui.basic.Label('Mail');
            me.__mail = new qx.ui.form.TextField();
            me.__mail.setMarginRight(10);
            mailLabel.setAlignY('middle');
            me.__details.add(mailLabel, {row: 0, column: 2});
            me.__details.add(me.__mail, {row: 0, column: 3, colSpan: 1});

            var foreLabel = new qx.ui.basic.Label('Forename');
            me.__foreName = new qx.ui.form.TextField();
            me.__foreName.setMarginRight(10);
            foreLabel.setAlignY('middle');
            me.__details.add(foreLabel, {row: 1, column: 0});
            me.__details.add(me.__foreName, {row: 1, column: 1, colSpan: 1});

            var surLabel = new qx.ui.basic.Label('Surname');
            me.__surName = new qx.ui.form.TextField();
            me.__surName.setMarginRight(10);
            surLabel.setAlignY('middle');
            me.__details.add(surLabel, {row: 1, column: 2});
            me.__details.add(me.__surName, {row: 1, column: 3, colSpan: 1});

            var activeLabel = new qx.ui.basic.Label('Active');
            activeLabel.setAlignY('middle');
            me.__active = new qx.ui.form.CheckBox();
            me.__details.add(activeLabel, {row: 2, column: 0});
            me.__details.add(me.__active, {row: 2, column: 1, colSpan: 1});

            var adminLabel = new qx.ui.basic.Label('Admin');
            adminLabel.setAlignY('middle');
            me.__admin = new qx.ui.form.CheckBox();
            me.__details.add(adminLabel, {row: 2, column: 2});
            me.__details.add(me.__admin, {row: 2, column: 3, colSpan: 1});

            var bottomBar = new qx.ui.container.Composite();
            var bottomBarLayout = new qx.ui.layout.HBox(5);
            bottomBar.setLayout(bottomBarLayout);
            bottomBar.setMargin(5, 0, 5, 0);
            bottomBar.setHeight(26);
            bottomBarLayout.setAlignX('right');

            me.__manageApps = new qx.ui.form.Button('Applications', 'everydesk/icons/16x16/application.png');
            me.__manageApps.setEnabled(false);
            me.__manageApps.addListener('execute', function(e) {
                var dialog = new everydesk.plugin.admin.ApplicationSelector();
                dialog.init();
                dialog.setUser(me.__currentUser);
                dialog.show();
            });
            bottomBar.add(me.__manageApps);
            var newUser = new qx.ui.form.Button('New', 'everydesk/icons/16x16/add.png');
            newUser.addListener('execute', function(evt) {
                me.__currentUser = {};
                me.__fillUserDetails({
                    username: '',
                    email: '',
                    forename: '',
                    surname: '',
                    active: true,
                    admin: false
                });
            });
            bottomBar.add(newUser);

            var save = new qx.ui.form.Button('Save', 'everydesk/icons/16x16/diskette.png');
            save.addListener('execute', function(evt) {
                console.log(evt);
                var user = me.__currentUser;
                user.username = me.__detailsName.getValue();
                user.email = me.__mail.getValue();
                user.surname = me.__surName.getValue();
                user.forename = me.__foreName.getValue();
                user.active = me.__active.getValue();
                user.admin = me.__admin.getValue();

                var request = new everydesk.base.io.Xhr('/user', 'POST');
                request.setRequestHeader(
                    'X-EVERYDESK-SESSION',
                    everydesk.base.system.Session.SESSIONKEY);
                request.setRequestHeader(
                    'Content-Type', 'application/json'
                );
                request.addListener('success', function(evt1) {
                    console.log(evt1);
                    me.__fetchUsers();
                });
                request.setRequestData(user);
                request.send();
            });
            bottomBar.add(save);

            var cancel = new qx.ui.form.Button('Cancel', 'everydesk/icons/16x16/cancel.png');
            cancel.addListener('execute', function() {
                me.__fillUserDetails(me.__currentUser);
            }, me);
            bottomBar.add(cancel);

            container.add(me.__details, {flex: 1});
            me.__details.add(bottomBar, {row: 3, column: 0, colSpan: 4});
            container.add(me.__details);//, {height: '40%'});
            this.__fetchUsers();
            return container;
        },

        __fillUserDetails: function(user) {
            this.__currentUser = user;
            this.__detailsName.setValue(user.username);
            this.__mail.setValue(user.email);
            this.__foreName.setValue(user.forename);
            this.__surName.setValue(user.surname);
            this.__active.setValue(user.active);
            this.__admin.setValue(user.admin);
            if (user.external) {
                this.__detailsName.setEnabled(false);
                this.__mail.setEnabled(false);
                this.__surName.setEnabled(false);
                this.__foreName.setEnabled(false);
            }
            else {
                this.__detailsName.setEnabled(true);
                this.__mail.setEnabled(true);
                this.__surName.setEnabled(true);
                this.__foreName.setEnabled(true);
            }
            if (user.username === everydesk.base.system.Session.USERNAME) {
                this.__manageApps.setEnabled(false);
                this.__admin.setEnabled(false);
            }
            else {
                this.__manageApps.setEnabled(true);
                this.__admin.setEnabled(true);
            }
        },

        __fetchUsers: function() {
            var me = this;
            var request = new everydesk.base.io.Xhr('/user', 'GET');
            request.setRequestHeader(
                'X-EVERYDESK-SESSION',
                everydesk.base.system.Session.SESSIONKEY);
            request.addListener('success', function(evt) {
                me.__users = evt.getTarget().getResponse();
                me.__userTableModel.removeRows(0, me.__userTableModel.getRowCount());
                console.log(me.__users);
                var userRows = [];
                for (var i = 0; i < me.__users.length; i++) {
                    userRows.push([
                        me.__users[i].username,
                        me.__users[i].forename,
                        me.__users[i].surname,
                        me.__users[i].active,
                        me.__users[i].admin,
                        me.__users[i].external,
                        me.__users[i].external || me.__users[i].username === everydesk.base.system.Session.USERNAME ? '' : 'everydesk/icons/16x16/delete.png',
                        me.__users[i].id
                    ]);
                }
                me.__userTableModel.setData(userRows);
            });
            request.send();
        },

        __loadApps: function() {
            var me = this;
            me.__apps = everydesk.base.system.StarterManager.getInstance().getApps();
            var container = new qx.ui.container.Composite();
            var layout = new qx.ui.layout.VBox();
            container.setLayout(layout);
            container.setPadding(0, 0, 0, 0);
            var rows = me.__createApplicationRows(me.__apps);
            me.__appTableModel = this._tableModel = new qx.ui.table.model.Simple();
            me.__appTableModel.setColumns(['', 'Name', 'Category', 'Group', 'Admin']);
            me.__appTableModel.setData(rows);
            var colModel = {
                tableColumnModel: function(obj) {
                    return new qx.ui.table.columnmodel.Resize(obj);
                }
            };
            var table = new qx.ui.table.Table(me.__appTableModel, colModel);
            table.setStatusBarVisible(false);
            table.setShowCellFocusIndicator(false);
            table.setFocusCellOnPointerMove(true);
            table.setColumnVisibilityButtonVisible(false);
            table.getDataRowRenderer().setHighlightFocusRow(true);
            table.getSelectionModel().setSelectionMode(2);
            table.setMaxHeight(300);

            table.addListener('cellTap', function(evt) {
                var row = evt.getRow();
                var app = me.__appTableModel.getValue(5, row);
                me.__groupName.setEnabled(true);
                me.__category.setEnabled(true);
                for (var i = 0; i < me.__apps.length; i++) {
                    if (me.__apps[i].id === app) {
                        me.__fillAppDetails(me.__apps[i]);
                    }
                }
            }, me);

            var tcm = table.getTableColumnModel();
            tcm.setDataCellRenderer(
                0,
                new qx.ui.table.cellrenderer.Image(16, 16));
            tcm.setDataCellRenderer(
                4,
                new qx.ui.table.cellrenderer.Boolean());
            tcm.getBehavior().setWidth(0, 26);
            tcm.getBehavior().setWidth(1, '1*');
            tcm.getBehavior().setWidth(2, '1*');
            tcm.getBehavior().setWidth(3, '1*');
            tcm.getBehavior().setWidth(4, 60);
            container.add(table, {flex: 1, height: '40%'});

            me.__detailsApp = new qx.ui.groupbox.GroupBox('Application');
            me.__detailsApp.setMargin(5, 0, 0, 0);
            var formLayout = new qx.ui.layout.Grid(0, 5);
            formLayout.setColumnWidth(0, 60);
            formLayout.setColumnWidth(1, 200);
            formLayout.setColumnWidth(2, 60);
            formLayout.setColumnWidth(3, 200);
            me.__detailsApp.setLayout(formLayout);


            var nameLabel = new qx.ui.basic.Label('Name');
            me.__detailsAppName = new qx.ui.form.TextField();
            me.__detailsAppName.setMarginRight(10);
            me.__detailsAppName.setEnabled(false);
            nameLabel.setAlignY('middle');
            me.__detailsApp.add(nameLabel, {row: 0, column: 0});
            me.__detailsApp.add(me.__detailsAppName, {row: 0, column: 1, colSpan: 1});

            var categoryLabel = new qx.ui.basic.Label('Category');
            me.__category = new qx.ui.form.ComboBox();
            for (var i = 0; i < me.__apps.length; i++) {
                if (!me.__apps[i].category) {
                    continue;
                }
                var children = me.__category.getChildren();
                var hasGroup = false;
                for (var j = 0; j < children.length; j++) {
                    if (children[j].getLabel() === me.__apps[i].category) {
                        hasGroup = true;
                        break;
                    }
                }
                if (!hasGroup) {
                    var item = new qx.ui.form.ListItem(me.__apps[i].category);
                    me.__category.add(item);
                }
            }
            me.__category.setEnabled(false);
            me.__category.setMarginRight(10);
            categoryLabel.setAlignY('middle');
            me.__detailsApp.add(categoryLabel, {row: 1, column: 0});
            me.__detailsApp.add(me.__category, {row: 1, column: 1, colSpan: 1});

            var groupLabel = new qx.ui.basic.Label('Group');
            me.__groupName = new qx.ui.form.ComboBox();
            for (var i = 0; i < me.__apps.length; i++) {
                if (!me.__apps[i].group) {
                    continue;
                }
                var children = me.__groupName.getChildren();
                var hasGroup = false;
                for (var j = 0; j < children.length; j++) {
                    if (children[j].getLabel() === me.__apps[i].group) {
                        hasGroup = true;
                        break;
                    }
                }
                if (!hasGroup) {
                    var item = new qx.ui.form.ListItem(me.__apps[i].group);
                    me.__groupName.add(item);
                }
            }
            me.__groupName.setEnabled(false);
            me.__groupName.setMarginRight(10);
            groupLabel.setAlignY('middle');
            me.__detailsApp.add(groupLabel, {row: 1, column: 2});
            me.__detailsApp.add(me.__groupName, {row: 1, column: 3, colSpan: 1});

            var adminLabel = new qx.ui.basic.Label('Admin');
            adminLabel.setAlignY('middle');
            me.__adminApp = new qx.ui.form.CheckBox();
            me.__detailsApp.add(adminLabel, {row: 2, column: 0});
            me.__detailsApp.add(me.__adminApp, {row: 2, column: 1, colSpan: 1});

            var bottomBar = new qx.ui.container.Composite();
            var bottomBarLayout = new qx.ui.layout.HBox(5);
            bottomBar.setLayout(bottomBarLayout);
            bottomBar.setMargin(5, 0, 5, 0);
            bottomBar.setHeight(26);
            bottomBarLayout.setAlignX('right');

            var save = new qx.ui.form.Button('Save', 'everydesk/icons/16x16/diskette.png');
            save.addListener('execute', function(evt) {
                console.log(evt);
                var app = me.__currentApp;
                app.category = me.__category.getValue();
                app.group = me.__groupName.getValue();
                app.admin = me.__adminApp.getValue();

                var request = new everydesk.base.io.Xhr('/starter', 'POST');
                request.setRequestHeader(
                    'X-EVERYDESK-SESSION',
                    everydesk.base.system.Session.SESSIONKEY);
                request.setRequestHeader(
                    'Content-Type', 'application/json'
                );
                request.addListener('success', function(evt1) {
                    console.log(evt1);
                    everydesk.base.system.StarterManager.getInstance().load();
                });
                request.setRequestData(app);
                request.send();
            });
            bottomBar.add(save);

            var cancel = new qx.ui.form.Button('Cancel', 'everydesk/icons/16x16/cancel.png');
            cancel.addListener('execute', function() {
                me.__fillAppDetails(me.__currentApp);
            }, me);
            bottomBar.add(cancel);

            me.__detailsApp.add(bottomBar, {row: 3, column: 0, colSpan: 4});
            container.add(me.__detailsApp, {flex: 1});
            return container;
        },

        __fillAppDetails: function(app) {
            this.__currentApp = app;
            this.__detailsAppName.setValue(app.name);
            this.__category.setValue(app.category);
            this.__groupName.setValue(app.group);
            this.__adminApp.setValue(app.admin);
        },

        __createApplicationRows: function(apps) {
            var rows = [];
            for (var i = 0; i < apps.length; i++) {
                rows.push([
                    'resource/everydesk/icons/16x16/'+apps[i].icon,
                    apps[i].name,
                    apps[i].category,
                    apps[i].group,
                    apps[i].admin,
                    apps[i].id
                ]);
            }
            return rows;
        },

        __refreshApps: function() {
            var me = this;
            me.__apps = everydesk.base.system.StarterManager.getInstance().getApps();
            var rows = me.__createApplicationRows(me.__apps);
            me.__appTableModel.removeRows(0, me.__apps.length);
            me.__appTableModel.setData(rows);
        }
    },

    construct: function() {
        var me = this;
        me.base(arguments);
        this.setWidth(600);
        this.setHeight(500);
        this.init();
        this.centerWindow();
        qx.event.message.Bus.subscribe('loadsuccessStarter', me.__refreshApps, me);
    }
});

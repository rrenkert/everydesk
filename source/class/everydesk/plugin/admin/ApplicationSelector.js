/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 */
qx.Class.define('everydesk.plugin.admin.ApplicationSelector', {
    extend: everydesk.plugin.Dialog,

    members: {
        __user: null,
        __apps: null,
        __source: null,
        __target: null,

        init: function() {
            var me = this;
            me.setCaption('User Applications');
            var mainLayout = new qx.ui.layout.VBox();
            me.setLayout(mainLayout);
            me.__apps = everydesk.base.system.StarterManager.getInstance().getApps();
            var container = new qx.ui.container.Composite();
            var layout = new qx.ui.layout.Grid();
            layout.setSpacing(5);
            layout.setColumnAlign(0, 'left', 'middle');
            layout.setColumnAlign(1, 'center', 'middle');
            layout.setColumnAlign(2, 'right', 'middle');
            container.setLayout(layout);
            container.setPadding(5, 5, 5, 5);
            me.add(container);

            var sourceLabel = new qx.ui.basic.Label('Available Apllications');
            var targetLabel = new qx.ui.basic.Label('User Apllications');
            container.add(sourceLabel, {row: 0, column: 0});
            container.add(targetLabel, {row: 0, column: 2});
            var oneLeft = new qx.ui.form.Button('<');
            var oneRight = new qx.ui.form.Button('>');
            var allRight = new qx.ui.form.Button('>>');
            var allLeft = new qx.ui.form.Button('<<');
            oneLeft.addListener('execute', me.__moveSelectedLeft, me);
            oneRight.addListener('execute', me.__moveSelectedRight, me);
            allLeft.addListener('execute', me.__moveAllLeft, me);
            allRight.addListener('execute', me.__moveAllRight, me);
            var buttonContainer = new qx.ui.container.Composite();
            var buttonLayout = new qx.ui.layout.VBox(5);
            buttonLayout.setAlignY('middle');
            buttonContainer.setLayout(buttonLayout);
            buttonContainer.add(oneRight);
            buttonContainer.add(oneLeft);
            buttonContainer.add(allRight);
            buttonContainer.add(allLeft);
            container.add(buttonContainer, {row: 1, column: 1});
            me.__source = new qx.ui.form.List();
            me.__source.setMinWidth(120);
            me.__source.setDraggable(true);
            me.__source.setDroppable(true);
            me.__source.setSelectionMode('multi');
            container.add(me.__source, {row: 1, column: 0});
            for (var i = 0; i < me.__apps.length; i++) {
                var item = new qx.ui.form.ListItem(me.__apps[i].name,
                    'resource/everydesk/icons/16x16/'+me.__apps[i].icon, me.__apps[i].id);
                me.__source.add(item);
            }
            me.__source.addListener('dragstart', me.__dragListener, me.__source);
            me.__source.addListener('droprequest', me.__dropRequestListener, me.__source);
            me.__source.addListener('drop', me.__dropListener, me.__source);
            me.__source.addListener('dragover', me.__dragOverListener, me.__source);

            me.__target = new qx.ui.form.List();
            me.__target.setMinWidth(120);
            me.__target.setDraggable(true);
            me.__target.setDroppable(true);
            me.__target.setSelectionMode('multi');
            container.add(me.__target, {row: 1, column: 2});
            me.__target.addListener('dragstart', me.__dragListener, me.__target);
            me.__target.addListener('droprequest', me.__dropRequestListener, me.__target);
            me.__target.addListener('drop', me.__dropListener, me.__target);
            me.__target.addListener('dragover', me.__dragOverListener, me.__target);

            var save = new qx.ui.form.Button('Save', 'everydesk/icons/16x16/diskette.png');
            save.addListener('execute', function(evt) {
                var data = [];
                var items = me.__target.getChildren();
                for (var i = 0; i < items.length; i++) {
                    for (var j = 0; j < me.__apps.length; j++) {
                        if (items[i].getModel() == me.__apps[j].id) {
                            data.push(me.__apps[j]);
                        }
                    }
                }
                var request = new everydesk.base.io.Xhr('/user/application/' + me.__user.id, 'POST');
                request.setRequestHeader(
                    'X-EVERYDESK-SESSION',
                    everydesk.base.system.Session.SESSIONKEY);
                request.setRequestHeader(
                    'Content-Type', 'application/json'
                );
                request.addListener('success', function(evt1) {
                    console.log(evt1);
                });
                request.setRequestData(data);
                request.send();
            });

            var cancel = new qx.ui.form.Button('Cancel', 'everydesk/icons/16x16/cancel.png');
            cancel.addListener('execute', function() {
                me.close();
            }, me);
            var bottomBar = new qx.ui.container.Composite();
            var bottomBarLayout = new qx.ui.layout.HBox(5);
            bottomBar.setLayout(bottomBarLayout);
            bottomBarLayout.setAlignX('right');
            bottomBar.add(save);
            bottomBar.add(cancel);

            container.add(bottomBar, {row: 2, column: 0, colSpan: 3});
        },

        __dragListener: function(e) {
            e.addType('value');
            e.addType('items');

            e.addAction('move');
        },

        __dropRequestListener: function(e) {
            var action = e.getCurrentAction();
            var type = e.getCurrentType();
            var result;
            var selection = this.getSelection();
            var dragTarget = e.getDragTarget();
            if (selection.length === 0) {
                selection.push(dragTarget);
            }
            else if (selection.indexOf(dragTarget) === -1) {
                selection = [dragTarget];
            }

            switch (type) {
            case 'items':
                result = selection;
                break;
            case 'value':
                result = selection[0].getLabel();
                break;
            default: break;
            }

            if (action === 'move') {
                for (var i = 0, l=selection.length; i<l; i++) {
                    this.remove(selection[i]);
                }
            }

            e.addData(type, result);
        },

        __dropListener: function(e) {
            var items = e.getData('items');
            for (var i = 0, l=items.length; i<l; i++) {
                this.add(items[i]);
            }
        },

        __dragOverListener: function(e) {
            if (!e.supportsType('items')) {
                e.preventDefault();
            }
        },

        __moveAllLeft: function() {
            while (this.__target.hasChildren()) {
                this.__source.add(this.__target.getChildren()[0]);
            }
        },

        __moveAllRight: function() {
            while (this.__source.hasChildren()) {
                this.__target.add(this.__source.getChildren()[0]);
            }
        },

        __moveSelectedLeft: function() {
            var selection = this.__target.getSelection();
            for (var i = selection.length - 1; i >= 0; i--) {
                this.__source.add(selection[i]);
            }
        },

        __moveSelectedRight: function() {
            var selection = this.__source.getSelection();
            for (var i = selection.length - 1; i >= 0; i--) {
                this.__target.add(selection[i]);
            }
        },

        setUser: function(user) {
            var me = this;
            me.__user = user;
            var request = new everydesk.base.io.Xhr('/user/application/' + user.id, 'GET');
            request.setRequestHeader(
                'X-EVERYDESK-SESSION',
                everydesk.base.system.Session.SESSIONKEY);
            request.addListener('success', function(evt) {
                me.__userApps = evt.getTarget().getResponse();
                console.log(me.__userApps);
                for (var i = 0; i < me.__userApps.length; i++) {
                    for (var j = me.__source.getChildren().length - 1; j >= 0; j--) {
                        if (me.__userApps[i].id == me.__source.getChildren()[j].getModel()) {
                            me.__target.add(me.__source.getChildren()[j]);
                            break;
                        }
                    }
                }
            });
            request.send();
        }
    },

    construct: function() {
        var me = this;
        me.base(arguments);
        //me.setWidth(400);
        me.setHeight(300);
        //me.init();
    }
});

/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 */
qx.Class.define('everydesk.plugin.settings.Settings', {
    extend: everydesk.plugin.Application,

    members: {
        __list: null,
        __page: null,
        __pages: [],

        init: function() {

            var me = this;
            me.setCaption('Settings');
            me.setContentPadding(0);
            var mainLayout = new qx.ui.layout.VBox();
            var splitContainer = new qx.ui.splitpane.Pane('horizontal');
            me.setLayout(mainLayout);
            me.__list = new qx.ui.form.List();
            me.__list.addListener('changeSelection', function(evt) {
                me.__page.removeAll();
                var ndx = 0;
                for (var i = 0; i < me.__pages.length; i++) {
                    if (me.__pages[i].name === evt.getData()[0].getLabel()) {
                        ndx = i;
                        break;
                    }
                }
                me.__page.add(me.__pages[ndx].widget, {flex: 1});
            });
            me.__page = new qx.ui.container.Composite();
            var containerLayout = new qx.ui.layout.VBox();
            me.__page.setLayout(containerLayout);
            splitContainer.add(me.__list, 1);
            splitContainer.add(me.__page, 2);
            me.add(splitContainer, {flex: 1});
            var apps = everydesk.base.system.StarterManager.getInstance().getApps();
            me.__globalSettings();
            this.__loadApps(apps);
        },
        getFileTypes: function() {
            return [];
        },

        __globalSettings: function() {
            var me = this;
            var container = new qx.ui.container.Composite();
            container.setMargin(5, 5, 5, 5);
            container.setLayout(new qx.ui.layout.VBox(5));
            var background = new qx.ui.groupbox.GroupBox('Background');
            var gridLayout = new qx.ui.layout.Grid(5, 5);
            gridLayout.setRowAlign(0, 'left', 'bottom');
            background.setLayout(gridLayout);

            me.__preview = new qx.ui.basic.Image();
            me.__preview.setWidth(Math.floor(window.innerWidth/10));
            me.__preview.setHeight(Math.floor(window.innerHeight/10));
            me.__preview.setBackgroundColor('rgb(255, 255, 255)');
            me.__preview.setScale(true);
            var sm = everydesk.base.system.SettingsManager.getInstance();
            var settings = sm.getSettings();
            if (settings.background !== '') {
                me.__preview.setSource(settings.background);
            }
            background.add(me.__preview, {row: 0, column: 0});
            var openButton = new qx.ui.form.Button('Auswählen', 'everydesk/icons/16x16/open_folder.png');
            openButton.setMaxHeight(26);
            openButton.addListener('execute', function() {
                var msg = new qx.event.message.Message(
                    'showdialog', {
                        endpoint: 'everydesk.plugin.filebrowser.FileOpen',
                        icon: 'everydesk/icons/16x16/folder.png',
                        name: 'Open file',
                        file: '/fs/',
                        callback: me.__setBackground
                    }
                );
                msg.setSender(me);
                qx.event.message.Bus.dispatch(msg);
            });
            var saveButton = new qx.ui.form.Button('Übernehmen', 'everydesk/icons/16x16/accept_button.png');
            saveButton.setMaxHeight(26);
            saveButton.addListener('execute', function() {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'changebackground', {
                        src: me.__preview.getSource()
                    }
                ));
                var smi = everydesk.base.system.SettingsManager.getInstance();
                var settingsLocal = smi.getSettings();
                settingsLocal.background = me.__preview.getSource();
                sm.updateSettings(settingsLocal);
            });
            background.add(openButton, {row: 0, column: 1});
            background.add(saveButton, {row: 0, column: 2});
            container.add(background);
            me.__addUI('Allgemein', container);
        },

        __setBackground: function(file) {
            everydesk.base.system.FileManager.getInstance().open(file, this.__doBackground, this);
        },

        __doBackground: function(success, file, content) {
            var reader = new FileReader();
            var me = this;
            reader.onload = function(e) {
                me.__preview.setSource(e.target.result);
            };
            console.log(file.get('mimeType'));
            reader.readAsDataURL(new Blob([content], {type: file.get('mimeType')}));
        },

        __buildSettings: function(apps) {
            qx.event.message.Bus.dispatch(new qx.event.message.Message(
                'actionend', {
                    type: 'plugin',
                    icon: 'everydesk/icons/16x16/application.png'
                }
            ));
            var mixinClass = qx.Mixin.getByName('everydesk.plugin.MSettings');
            for (var i = 0; i < apps.length; i++) {
                var appClass = qx.Class.getByName(apps[i].endpoint);
                if (qx.Class.hasMixin(appClass, mixinClass)) {
                    var ndx = apps[i].endpoint.lastIndexOf('.');
                    var settingsName = apps[i].endpoint.substring(0, ndx);
                    var clazz = qx.Class.getByName(settingsName + '.Settings');
                    if (!clazz) {
                        console.log('No settings found for ' + settingsName + 'Settings');
                        // show error
                        return;
                    }
                    var settings = new clazz();
                    this.__addUI(apps[i].name, settings.getUI());
                }
            }
            var first = this.__list.getChildren()[0];
            if (first) {
                this.__list.setSelection([first]);
            }
        },

        __addUI: function(name, widget) {
            var item = new qx.ui.form.ListItem(name);
            this.__list.add(item);
            this.__pages.push({name: name, widget: widget});
        },

        __findEndpoint: function(endpoint) {
            var parts = qx.io.PartLoader.getInstance().getParts();
            // strip endpoint class
            var ndx = endpoint.lastIndexOf('.');
            endpoint = endpoint.substring(0, ndx);
            for (var key in parts) {
                if (key.indexOf(endpoint) >= 0) {
                    return key;
                }
            }
            return endpoint;
        },

        __loadApps: function(apps) {
            var me = this;
            var toLoad = [];
            for (var i = 0; i < apps.length; i++) {
                if (apps[i].type.name === 'app') {
                    if (!qx.Class.isDefined(apps[i].endpoint)) {
                        var endpoint = me.__findEndpoint(apps[i].endpoint);
                        toLoad.push(endpoint);
                    }
                }
            }
            if (toLoad.length > 0) {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionstart', {
                        type: 'plugin',
                        icon: 'everydesk/icons/16x16/application.png'
                    }
                ));
                var size = toLoad.length;
                qx.io.PartLoader.require(toLoad, function(state) {
                    for (var j = 0; j < state.length; j++) {
                        if (state[j] === 'complete') {
                            size--;
                        }
                    }
                    if (size === 0) {
                        me.__buildSettings(apps);
                    }
                }, me);
            }
            else {
                me.__buildSettings(apps);
            }
        }
    },

    construct: function() {
        this.base(arguments);
        this.setWidth(600);
        this.setHeight(500);
        this.init();
    }
});

/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 */
qx.Class.define('everydesk.plugin.favoritemanager.NewFavorite', {
    extend: everydesk.plugin.Dialog,

    members: {
        __fileField: null,
        __file: null,

        init: function() {
            var me = this;
            me.setContentPadding(5);
            var formContainer = new qx.ui.container.Composite();
            var formLayout = new qx.ui.layout.Grid(0, 5);
            me.setLayout(new qx.ui.layout.VBox());
            formLayout.setColumnFlex(1, 1);
            formLayout.setColumnWidth(0, 60);
            formContainer.setLayout(formLayout);

            var nameLabel = new qx.ui.basic.Label('Name');
            var nameField = new qx.ui.form.TextField();
            nameLabel.setAlignY('middle');
            formContainer.add(nameLabel, {row: 0, column: 0});
            formContainer.add(nameField, {row: 0, column: 1, colSpan: 2});
            var typeLabel = new qx.ui.basic.Label('Type');
            typeLabel.setAlignY('middle');
            typeLabel.setMarginRight(5);
            var typeFile = new qx.ui.form.ListItem('File', null, '1');
            var typeURL = new qx.ui.form.ListItem('URL', null, '2');
            var type = new qx.ui.form.SelectBox();
            type.add(typeFile);
            type.add(typeURL);
            type.addListener('changeSelection', function(evt) {
                console.log(evt.getData()[0].get('model'));
                var select = evt.getData()[0].get('model');
                if (select == 1) {
                    console.log('file');
                    this.__fileField.setEnabled(true);
                    openButton.setEnabled(true);
                    fileLabel.setEnabled(true);
                    urlField.setEnabled(false);
                    urlLabel.setEnabled(false);
                }
                else {
                    console.log('url');
                    this.__fileField.setEnabled(false);
                    openButton.setEnabled(false);
                    fileLabel.setEnabled(false);
                    urlField.setEnabled(true);
                    urlLabel.setEnabled(true);
                }
            }, me);
            formContainer.add(typeLabel, {row: 1, column: 0});
            formContainer.add(type, {row: 1, column: 1, colSpan: 2});

            var fileLabel = new qx.ui.basic.Label('File');
            fileLabel.setAlignY('middle');
            me.__fileField = new qx.ui.form.TextField();
            me.__fileField.setMarginRight(5);
            me.__fileField.setAlignY('middle');
            var openButton = new qx.ui.form.Button(null, 'everydesk/icons/16x16/open_folder.png');
            openButton.addListener('execute', function() {
                var msg = new qx.event.message.Message(
                    'showdialog', {
                        endpoint: 'everydesk.plugin.filebrowser.FileOpen',
                        icon: 'everydesk/icons/16x16/folder.png',
                        name: 'Open file',
                        file: '/fs/'
                    }
                );
                msg.setSender(me);
                qx.event.message.Bus.dispatch(msg);
            });
            formContainer.add(fileLabel, {row: 2, column: 0});
            formContainer.add(me.__fileField, {row: 2, column: 1});
            formContainer.add(openButton, {row: 2, column: 2});

            var urlLabel = new qx.ui.basic.Label('URL');
            urlLabel.setAlignY('middle');
            urlLabel.setEnabled(false);
            var urlField = new qx.ui.form.TextField();
            urlField.setEnabled(false);
            formContainer.add(urlLabel, {row: 3, column: 0});
            formContainer.add(urlField, {row: 3, column: 1, colSpan: 2});

            var groupLabel = new qx.ui.basic.Label('Group');
            groupLabel.setAlignY('middle');
            var favManager = everydesk.base.system.FavoritesManager.getInstance();
            var favs = favManager.getFavorites();
            var groupBox = new qx.ui.form.ComboBox();
            for (var i = 0; i < favs.length; i++) {
                if (!favs[i].group) {
                    continue;
                }
                var children = groupBox.getChildren();
                var hasGroup = false;
                for (var j = 0; j < children.length; j++) {
                    if (children[j].getLabel() === favs[i].group) {
                        hasGroup = true;
                        break;
                    }
                }
                if (!hasGroup) {
                    var item = new qx.ui.form.ListItem(favs[i].group);
                    groupBox.add(item);
                }
            }
            formContainer.add(groupLabel, {row: 4, column: 0});
            formContainer.add(groupBox, {row: 4, column: 1, colSpan: 2});

            var save = new qx.ui.form.Button('Save');
            save.addListener('execute', function(evt) {
                console.log(evt);
                var favtype;
                var endpoint;
                var uri;
                var icon;
                if (type.getSelection()[0].get('model') === '1') {
                    favtype = {id: 2, name: 'file'};
                    endpoint = 'find_enpoint_todo';
                    uri = this.__file.get('path');
                    icon = 'everydesk/icons/16x16/' + everydesk.base.system.FileSystem.FILEICONS[this.__file.getMimeType()];
                    if (!icon) {
                        icon = 'everydesk/icons/16x16/page.png';
                    }
                }
                else {
                    favtype = {id: 3, name: 'url'};
                    endpoint = '_blank';
                    uri = urlField.getValue();
                    icon = 'everydesk/icons/16x16/monitor_link.png';
                }
                var content = {
                    type: favtype,
                    name: nameField.getValue(),
                    endpoint: endpoint,
                    group: groupBox.getValue(),
                    uri: uri,
                    icon: icon
                };
                everydesk.base.system.FavoritesManager.getInstance().addFavorite(content);
/*                var request = new everydesk.base.io.Xhr('/favorites', 'POST');
                request.setRequestHeader(
                    'X-EVERYDESK-SESSION',
                    everydesk.base.system.Session.SESSIONKEY);
                request.setRequestHeader(
                    'Content-Type', 'application/json'
                );
                request.addListener('success', function(evt) {
                    console.log(evt);
                });
                request.addListener('fail', function() {
                });
                request.setRequestData(content);
                request.send();*/
            }, me);
            var cancel = new qx.ui.form.Button('Cancel');
            cancel.addListener('execute', function() {
                this.close();
            }, me);
            me.add(formContainer, {flex: 1});

            var buttonContainer = new qx.ui.container.Composite();
            var buttonLayout = new qx.ui.layout.HBox(5);
            buttonContainer.setMarginTop(5);
            buttonLayout.setAlignX('right');
            buttonContainer.setLayout(buttonLayout);
            buttonContainer.add(save);
            buttonContainer.add(cancel);
            me.add(buttonContainer);
        },

        open: function(file) {
            if (file) {
                this.__fileField.setValue(file.get('name'));
                this.__file = file;
            }
        }
    },
    construct: function() {
        var me = this;
        me.base(arguments);
        me.setWidth(300);
        me.setHeight(150);
        me.init();
    }
});

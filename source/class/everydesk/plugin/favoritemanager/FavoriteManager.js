/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @require(everydesk.plugin.favoritemanager.NewFavorite)
 */
qx.Class.define('everydesk.plugin.favoritemanager.FavoriteManager', {
    extend: everydesk.plugin.Application,

    members: {
        __list: null,
        __listModel: null,
        __tree: null,
        __details: null,
        __detailsFile: null,
        __detailsName: null,
        __detailsType: null,
        __detailsUrl: null,
        __detailsGroup: null,
        __openButton: null,
        __fileLabel: null,
        __urlLabel: null,
        __currentFavorite: null,

        init: function() {
            var me = this;
            me.setCaption('Favorites');
            me.setContentPadding(0);
            me.setLayout(new qx.ui.layout.HBox());
            var splitPane = new qx.ui.splitpane.Pane('horizontal');
            me.add(splitPane, {flex: 1});
            me.__tree = new qx.ui.tree.Tree();
            var root = new qx.ui.tree.TreeFolder('Favorites');
            me.__tree.setRoot(root);
            root.setIcon('everydesk/icons/16x16/star.png');
            root.setOpen(true);
            root.setUserData('isRoot', true);
            root.addListener('click', me.__buildListListener, me);

            splitPane.add(me.__tree, 1);

            var container = new qx.ui.container.Composite();
            var layout = new qx.ui.layout.VBox(5);
            container.setLayout(layout);
            splitPane.add(container, 2);
            var colModel = {
                tableColumnModel: function(obj) {
                    return new qx.ui.table.columnmodel.Resize(obj);
                }
            };
            me.__listModel = new qx.ui.table.model.Simple();
            me.__listModel.setColumns(['icon', 'name', 'remove']);
            me.__listModel.setColumns(['', 'Name', '']);
            me.__listModel.setColumnEditable(0, false);
            me.__listModel.setColumnEditable(1, false);
            me.__listModel.setColumnEditable(2, false);
            me.__listModel.setColumnSortable(0, false);
            me.__listModel.setColumnSortable(2, false);
            var renderer = new qx.ui.table.cellrenderer.Image(16, 16);
            renderer.setRepeat('scale');
            me.__list = new qx.ui.table.Table(me.__listModel, colModel);
            var tcm = me.__list.getTableColumnModel();
            tcm.setDataCellRenderer(0, renderer);
            tcm.setDataCellRenderer(2, renderer);
            tcm.getBehavior().setWidth(0, 26);
            tcm.getBehavior().setWidth(1, '1*');
            tcm.getBehavior().setWidth(2, 26);
            me.__list.addListener('cellTap', function(evt) {
                var row = evt.getRow();
                var col = evt.getColumn();
                console.log(row + ' - ' + col);
                var fav = me.__listModel.getValue(3, row);
                if (col === 2) {
                    everydesk.base.system.FavoritesManager.getInstance().deleteFavorite(fav);
                    me.__clearDetails();
                }
                else {
                    me.__showDetails(fav);
                }
            }, me);
            me.__list.setStatusBarVisible(false);
            me.__list.setShowCellFocusIndicator(false);
            me.__list.setFocusCellOnPointerMove(true);
            me.__list.setColumnVisibilityButtonVisible(false);
            me.__list.getDataRowRenderer().setHighlightFocusRow(true);
            container.add(me.__list, {
                height: '40%'
            });

            me.__details = new qx.ui.groupbox.GroupBox('Favorite');
            me.__details.setMargin(0, 5, 0, 5);
            var formLayout = new qx.ui.layout.Grid(0, 5);
            formLayout.setColumnFlex(1, 1);
            formLayout.setColumnWidth(0, 60);
            me.__details.setLayout(formLayout);

            var nameLabel = new qx.ui.basic.Label('Name');
            me.__detailsName = new qx.ui.form.TextField();
            nameLabel.setAlignY('middle');
            me.__details.add(nameLabel, {row: 0, column: 0});
            me.__details.add(me.__detailsName, {row: 0, column: 1, colSpan: 2});
            var typeLabel = new qx.ui.basic.Label('Type');
            typeLabel.setAlignY('middle');
            typeLabel.setMarginRight(5);
            var typeFile = new qx.ui.form.ListItem('File', null, '2');
            var typeURL = new qx.ui.form.ListItem('URL', null, '3');
            me.__detailsType = new qx.ui.form.SelectBox();
            me.__detailsType.add(typeFile);
            me.__detailsType.add(typeURL);
            me.__detailsType.addListener('changeSelection', function(evt) {
                var select = evt.getData()[0].get('model');
                if (select == 2) {
                    console.log('file');
                    me.__detailsFile.setEnabled(true);
                    me.__openButton.setEnabled(true);
                    me.__fileLabel.setEnabled(true);
                    me.__detailsUrl.setEnabled(false);
                    me.__urlLabel.setEnabled(false);
                }
                else {
                    console.log('url');
                    me.__detailsFile.setEnabled(false);
                    me.__openButton.setEnabled(false);
                    me.__fileLabel.setEnabled(false);
                    me.__detailsUrl.setEnabled(true);
                    me.__urlLabel.setEnabled(true);
                }
            }, this);
            me.__details.add(typeLabel, {row: 1, column: 0});
            me.__details.add(me.__detailsType, {row: 1, column: 1, colSpan: 2});

            me.__fileLabel = new qx.ui.basic.Label('File');
            me.__fileLabel.setAlignY('middle');
            me.__detailsFile = new qx.ui.form.TextField();
            me.__detailsFile.setMarginRight(5);
            me.__detailsFile.setAlignY('middle');
            me.__openButton = new qx.ui.form.Button(null, 'everydesk/icons/16x16/open_folder.png');
            me.__openButton.addListener('execute', function() {
                var msg = new qx.event.message.Message(
                    'showdialog', {
                        endpoint: 'everydesk.plugin.filebrowser.FileOpen',
                        icon: 'everydesk/icons/16x16/folder.png',
                        name: 'Open file',
                        file: '/fs/'
                    }
                );
                msg.setSender(me);
                qx.event.message.Bus.dispatch(msg);
            });
            me.__details.add(me.__fileLabel, {row: 2, column: 0});
            me.__details.add(me.__detailsFile, {row: 2, column: 1});
            me.__details.add(me.__openButton, {row: 2, column: 2});

            me.__urlLabel = new qx.ui.basic.Label('URL');
            me.__urlLabel.setAlignY('middle');
            me.__urlLabel.setEnabled(false);
            me.__detailsUrl = new qx.ui.form.TextField();
            me.__detailsUrl.setEnabled(false);
            me.__details.add(me.__urlLabel, {row: 3, column: 0});
            me.__details.add(me.__detailsUrl, {row: 3, column: 1, colSpan: 2});

            var groupLabel = new qx.ui.basic.Label('Group');
            groupLabel.setAlignY('middle');
            var favManager = everydesk.base.system.FavoritesManager.getInstance();
            var favs = favManager.getFavorites();
            me.__detailsGroup = new qx.ui.form.ComboBox();
            for (var i = 0; i < favs.length; i++) {
                if (!favs[i].group) {
                    continue;
                }
                var children = me.__detailsGroup.getChildren();
                var hasGroup = false;
                for (var j = 0; j < children.length; j++) {
                    if (children[j].getLabel() === favs[i].group) {
                        hasGroup = true;
                        break;
                    }
                }
                if (!hasGroup) {
                    var item = new qx.ui.form.ListItem(favs[i].group);
                    me.__detailsGroup.add(item);
                }
            }
            me.__details.add(groupLabel, {row: 4, column: 0});
            me.__details.add(me.__detailsGroup, {row: 4, column: 1, colSpan: 2});

            var save = new qx.ui.form.Button('Save', 'everydesk/icons/16x16/diskette.png');
            save.addListener('execute', function(evt) {
                console.log(evt);
                var favtype;
                var endpoint;
                var uri;
                if (me.__detailsType.getSelection()[0].get('model') === '1') {
                    favtype = {id: 2, name: 'file'};
                    endpoint = 'find_enpoint_todo';
                    uri = this.__file.get('path');
                }
                else {
                    favtype = {id: 3, name: 'url'};
                    endpoint = '_blank';
                    uri = me.__detailsUrl.getValue();
                }
                var content = {
                    id: me.__currentFavorite.id,
                    icon: me.__currentFavorite.icon,
                    type: favtype,
                    name: me.__detailsName.getValue(),
                    endpoint: endpoint,
                    group: me.__detailsGroup.getValue(),
                    uri: uri
                };
                everydesk.base.system.FavoritesManager.getInstance().updateFavorite(content);
            }, me);
            var cancel = new qx.ui.form.Button('Close', 'everydesk/icons/16x16/cancel.png');
            cancel.addListener('execute', function() {
                this.close();
            }, me);
            container.add(me.__details, {flex: 1});

            var bottomBar = new qx.ui.container.Composite();
            var bottomBarLayout = new qx.ui.layout.HBox(5);
            bottomBar.setLayout(bottomBarLayout);
            bottomBar.setMargin(5, 5, 5, 5);
            var newButton = new qx.ui.form.Button('New', 'everydesk/icons/16x16/add.png');

            newButton.addListener('execute', function() {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'appstart', {
                        type: 'dialog',
                        endpoint: 'everydesk.plugin.favoritemanager.NewFavorite',
                        name: 'New Favorite',
                        icon: 'star.png',
                        file: null
                    }
                ));
            });
            bottomBar.add(newButton);
            var buttonContainer = new qx.ui.container.Composite();
            var buttonLayout = new qx.ui.layout.HBox(5);
            buttonLayout.setAlignX('right');
            buttonContainer.setLayout(buttonLayout);
            buttonContainer.add(save);
            buttonContainer.add(cancel);
            bottomBar.add(buttonContainer, {
                flex: 1
            });
            container.add(bottomBar);
            me.__refresh();
        },

        __refresh: function() {
            var selection = this.__tree.getSelection()[0];
            if (!selection) {
                selection = this.__tree.getRoot();
            }
            var name = selection.getUserData('name');
            if (selection.getUserData('isRoot')) {
                name = '';
            }
            this.__buildTree(this.__tree.getRoot());
            if (name !== '') {
                var children = this.__tree.getRoot().getChildren();
                for (var j = 0; j < children.length; j++) {
                    if (children[j].getUserData('name') === name) {
                        this.__buildList(children[j]);
                        this.__tree.setSelection(children[j]);
                        return;
                    }
                }
            }
            this.__buildList(this.__tree.getRoot());
            this.__tree.setSelection(this.__tree.getRoot());
        },

        __buildTree: function(root) {
            var me = this;
            root.removeAll();
            var favs = everydesk.base.system.FavoritesManager.getInstance().getFavorites();
            for (var i = 0; i < favs.length; i++) {
                if (favs[i].group) {
                    var children = root.getChildren();
                    var hasGroup = false;
                    for (var j = 0; j < children.length; j++) {
                        if (children[j].getUserData('name') === favs[i].group) {
                            hasGroup = true;
                            break;
                        }
                    }
                    if (!hasGroup) {
                        var item = new qx.ui.tree.TreeFile(favs[i].group);
                        item.setUserData('name', favs[i].group);
                        item.setIcon('everydesk/icons/16x16/folder.png');
                        root.add(item);
                        item.addListener('click', me.__buildListListener, me);
                    }
                }
            }
            me.__buildList(root);
        },

        __buildListListener: function(evt) {
            this.__buildList(evt.getTarget());
        },

        __buildList: function(treeItem) {
            console.log(treeItem);
            this.__listModel.removeRows(0, this.__listModel.getRowCount());
            var name = treeItem.getUserData('name');
            if (treeItem.getUserData('isRoot')) {
                name = '';
            }

            var favs = everydesk.base.system.FavoritesManager.getInstance().getFavorites();
            var rows = [];
            for (var i = 0; i < favs.length; i++) {
                if (favs[i].group === name) {
                    rows.push([favs[i].icon, favs[i].name, 'everydesk/icons/16x16/delete.png', favs[i]]);
                }
            }
            this.__listModel.setData(rows);
        },

        __showDetails: function(fav) {
            var me = this;
            me.__currentFavorite = fav;
            me.__detailsName.setValue(fav.name);
            me.__detailsGroup.setValue(fav.group);
            var selectables = me.__detailsType.getSelectables(true);
            var select;
            for (var i = 0; i < selectables.length; i++) {
                if (selectables[i].getModel() == fav.type.id) {
                    select = selectables[i];
                    break;
                }
            }
            me.__detailsType.setSelection([select]);
            if (select.getModel() === '2') {
                me.__detailsFile.setValue(fav.uri);
                me.__detailsUrl.resetValue();
                me.__detailsFile.setEnabled(true);
                me.__openButton.setEnabled(true);
                me.__fileLabel.setEnabled(true);
                me.__detailsUrl.setEnabled(false);
                me.__urlLabel.setEnabled(false);
            }
            else if (select.getModel() === '3') {
                me.__detailsUrl.setValue(fav.uri);
                me.__detailsFile.resetValue();
                me.__detailsFile.setEnabled(false);
                me.__openButton.setEnabled(false);
                me.__fileLabel.setEnabled(false);
                me.__detailsUrl.setEnabled(true);
                me.__urlLabel.setEnabled(true);
            }
        },

        __clearDetails: function() {
            var me = this;
            me.__detailsName.resetValue();
            me.__detailsGroup.resetValue();
            me.__detailsFile.resetValue();
            me.__detailsUrl.resetValue();
            me.__detailsFile.setEnabled(true);
            me.__openButton.setEnabled(true);
            me.__fileLabel.setEnabled(true);
            me.__detailsUrl.setEnabled(false);
            me.__urlLabel.setEnabled(false);
        }
    },

    construct: function() {
        var me = this;
        me.base(arguments);
        me.setWidth(600);
        me.setHeight(400);
        me.init();
        me.centerWindow();
        qx.event.message.Bus.subscribe('loadsuccessFavorites', me.__refresh, me);
    }
});

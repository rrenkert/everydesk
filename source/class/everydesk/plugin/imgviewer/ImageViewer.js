/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 */
qx.Class.define('everydesk.plugin.imgviewer.ImageViewer', {
    extend: everydesk.plugin.Application,

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        __fileTypes: [
            ['Image JPEG', '.*.jpg$'],
            ['Image PNG', '.*.png$']
        ],

        __image: null,
        __ratio: 1,

        init: function() {
            var me = this;
            me.setCaption('ImageViewer');
            me.setContentPadding(0);
            var toolbar = new qx.ui.toolbar.ToolBar();
            toolbar.setSpacing(3);
            me.add(toolbar);
            var tbPart1 = new qx.ui.toolbar.Part();
            var openButton = new qx.ui.toolbar.Button('Open', 'everydesk/icons/16x16/open_folder.png');
            openButton.addListener('execute', function() {
                var msg = new qx.event.message.Message(
                    'showdialog', {
                        endpoint: 'everydesk.plugin.filebrowser.FileOpen',
                        icon: 'everydesk/icons/16x16/folder.png',
                        name: 'Open file',
                        file: '/fs/'
                    }
                );
                msg.setSender(me);
                qx.event.message.Bus.dispatch(msg);
            });
            tbPart1.add(openButton);
            toolbar.add(tbPart1);
            tbPart1.setShow('icon');
            var mainContainer = new qx.ui.container.Composite();
            var contLayout = new qx.ui.layout.VBox();
            mainContainer.setLayout(contLayout);
            me.__image = new everydesk.plugin.imgviewer.Image();
            var layout = new qx.ui.layout.VBox();
            me.setLayout(layout);
            mainContainer.add(me.__image, {
                flex: 1
            });
            me.add(mainContainer, {flex: 1});
        },

        open: function(file) {
            var me = this;
            if (!file) {
                return;
            }
            if (file.getType() === 'file') {
                var filemanager = everydesk.base.system.FileManager.getInstance();
                filemanager.open(file, me.renderImage, me);
            }
        },

        renderImage: function(success, file, content) {
            if (!success) {
                return;
            }
            this.__image.render(file, content);
        },

        getFileTypes: function() {
            return this.__fileTypes;
        }
    },

    construct: function() {
        var me = this;
        me.base(arguments);
        me.setWidth(600);
        me.setHeight(400);
        me.init();
        me.centerWindow();
    }
});

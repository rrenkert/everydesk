/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
* @asset(lib/*)
 */
qx.Class.define('everydesk.plugin.imgviewer.Image', {
    extend: qx.ui.core.Widget,

    members: {
        __blob: null,
        __image: null,
        __meta: null,
        __element: null,

        init: function() {
            var me = this;
            me.addListener('resize', me.__onResize, me);
        },

        render: function(file, content) {
            var me = this;
            me.__element = me.getContentElement();
//            var data = new Uint8Array(this.response);
            me.__blob = new Blob([content], {type: 'image/jpeg'});
            loadImage.parseMetaData(me.__blob, function(meta) {
                me.__meta = meta;
                loadImage(me.__blob, function(img) {
                    var canvas = new qx.html.Canvas();
                    me.__image = img;
                    canvas.useElement(img);
                    me.__element.add(canvas);
                    me.__onResize();
                }, {
                    orientation: me.__meta.exif.get('Orientation'),
                    canvas: true
                });
            });
        },

        setSource: function(url) {
            var me = this;
            // url = url + '?EVERYDESK-SESSION=' +
            // everydesk.base.system.Session.SESSIONKEY
            me.__element = me.getContentElement();
            // var height = me.getHeight();
            // var width = me.getWidth();
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.setRequestHeader(
                'X-EVERYDESK-SESSION',
                everydesk.base.system.Session.SESSIONKEY);
            xhr.responseType = 'arraybuffer';
            xhr.onload = function() {
                if (this.status === 200) {
                    var data = new Uint8Array(this.response);
                    me.__blob = new Blob([data], {type: 'image/jpeg'});
                    loadImage.parseMetaData(me.__blob, function(meta) {
                        me.__meta = meta;
                        loadImage(me.__blob, function(img) {
                            var content = new qx.html.Canvas();
                            me.__image = img;
                            content.useElement(img);
                            me.__element.add(content);
                            me.__onResize();
                        }, {
                            orientation: me.__meta.exif.get('Orientation'),
                            canvas: true
                        });
                    });
                }
            };
            xhr.send();
        },

        __onResize: function() {
            var me = this;
            if (!me.__meta) {
                return;
            }
            var width = me.__element.getStyle('width');
            var height = me.__element.getStyle('height');
            width = parseInt(width.replace('px', ''), 10);
            height = parseInt(height.replace('px', ''), 10);
            var img = loadImage.scale(me.__image, {
                maxWidth: width,
                maxHeight: height
            });
            me.__element.removeAll();
            var content = new qx.html.Canvas();
            content.useElement(img);
            me.__element.add(content);
        }
    },

    construct: function() {
        this.base(arguments);
        this.init();
    }
});

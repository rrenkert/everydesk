/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/background/*)
 * @asset(everydesk/icons/16x16/*)
 * @asset(everydesk/icons/32x32/*)
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.plugin.Dialog', {
    extend: qx.ui.window.Window,
    type: 'abstract',

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        state: null,
        __referer: null,
        __callback: null,

        init: function() {
            var me = this;
            me.debug('Initialize Dialog');
        },

        open: function() {},
        setReferer: function(referer) {
            this.__referer = referer;
        },
        setCallback: function(callback) {
            this.__callback = callback;
        }
    },

    construct: function() {
        var me = this;
        me.base(arguments);
        me.setShowMinimize(false);
        me.setShowMaximize(false);
        me.setContentPadding(0, 0, 0, 0);
        me.setWidth(300);
        me.setHeight(300);
        me.center();
    }
});

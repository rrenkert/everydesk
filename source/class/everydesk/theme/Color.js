/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

qx.Theme.define("everydesk.theme.Color",
{
  extend : qx.theme.modern.Color,

  colors :
  {
    'window-caption-active-start': qx.core.Environment.get("css.rgba") ? "rgba(80, 80, 80, 0.6)" : "#666666",
    'window-caption-active-end': qx.core.Environment.get("css.rgba") ? "rgba(170, 170, 170, 0.9)" : "#666666",
    'window-caption-inactive-start': qx.core.Environment.get("css.rgba") ? "rgba(0, 0, 0, 0.4)" : "#666666",
    'window-caption-inactive-end': qx.core.Environment.get("css.rgba") ? "rgba(100, 100, 100, 0.7)" : "#666666",
    'window-active-caption-text': qx.core.Environment.get("css.rgba") ? "rgba(0, 0, 0, 0.9)" : "#666666",
    'window-inactive-caption-text': qx.core.Environment.get("css.rgba") ? "rgba(0, 0, 0, 0.7)" : "#666666"
  }
});

/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

qx.Theme.define('everydesk.theme.Decoration', {
    extend: qx.theme.modern.Decoration,

    decorations: {
        'taskbar': {
            style: {
                backgroundColor: 'rgba(0,0,0, 0.5)',
                colorTop: 'rgb(100, 100, 100)',
                style: 'solid',
                width: 1
            }
        },
        'tray': {
            style: {
                backgroundColor: 'rgba(0,0,0, 0.5)',
                colorTop: 'rgb(100, 100, 100)',
                style: 'solid',
                width: 1
            }
        },
        'clock': {
            style: {
                backgroundColor: 'rgba(0,0,0, 0.5)',
                colorTop: 'rgb(100, 100, 100)',
                style: 'solid',
                width: 1
            }
        },
        'actionField': {
            style: {
                backgroundColor: 'rgba(0,0,0, 0.5)',
                colorTop: 'rgb(100, 100, 100)',
                style: 'solid',
                width: 1,
                radiusTopLeft: 30
            }
        },
        'mainmenu': {
            style: {
                backgroundColor: 'rgba(0,0,0, 0.5)',
                colorRight: 'rgb(100, 100, 100)',
                style: 'solid',
                width: 1
            }
        },
        'mainmenubutton': {
            style: {
                radius: 3,
                color: 'rgb(100, 100, 100)',
                width: 1,
                startColor: 'rgba(0,0,0,0.2)',
                endColor: 'rgba(100, 100, 100, 0.7)',
                startColorPosition: 35,
                endColorPosition: 100
            }
        },
        'mainmenubutton-pressed': {
            style: {
                radius: 3,
                color: 'rgb(100, 100, 100)',
                width: 1,
                startColor: 'rgba(100,100,100,0.7)',
                endColor: 'rgba(0, 0, 0, 0.2)',
                startColorPosition: 35,
                endColorPosition: 100
            }
        },
        'startermenu': {
            style: {
                startColor: 'rgba(0,0,0,0.2)',
                endColor: 'rgba(100, 100, 100, 0.7)',
                startColorPosition: 35,
                endColorPosition: 100,
                width: 1,
                color: 'rgb(100, 100, 100)',
                radiusTopRight: 30,
                radiusBottomRight: 30
            }
        },
        'startermenu-pressed': {
            style: {
                startColor: 'rgba(100,100,100,0.7)',
                endColor: 'rgba(0, 0, 0, 0.2)',
                startColorPosition: 35,
                endColorPosition: 100,
                width: 1,
                color: 'rgb(100, 100, 100)',
                radiusTopRight: 30,
                radiusBottomRight: 30
            }
        },
        'startermenu-hovered': {
            style: {
                startColor: 'rgba(70,70,70,0.2)',
                endColor: 'rgba(150, 150, 150, 0.7)',
                startColorPosition: 35,
                endColorPosition: 100,
                width: 1,
                color: 'rgb(100, 100, 100)',
                radiusTopRight: 30,
                radiusBottomRight: 30
            }
        },
        'starterbutton': {
            style: {
                startColor: 'rgba(0,0,0,0.2)',
                endColor: 'rgba(100, 100, 100, 0.7)',
                startColorPosition: 35,
                endColorPosition: 100,
                width: 1,
                radius: 3,
                color: 'rgb(100, 100, 100)'
            }
        },
        'starterbutton-pressed': {
            style: {
                startColor: 'rgba(100,100,100,0.7)',
                endColor: 'rgba(0, 0, 0, 0.2)',
                startColorPosition: 35,
                endColorPosition: 100,
                width: 1,
                color: 'rgb(100, 100, 100)'
            }
        },
        'starterbutton-hovered': {
            style: {
                startColor: 'rgba(70,70,70,0.2)',
                endColor: 'rgba(150, 150, 150, 0.7)',
                startColorPosition: 35,
                endColorPosition: 100,
                width: 1,
                color: 'rgb(100, 100, 100)'
            }
        },
        'taskbutton': {
            style: {
                startColor: 'rgba(100,100,100,0.7)',
                endColor: 'rgba(0, 0, 0, 0.2)',
                startColorPosition: 35,
                endColorPosition: 100,
                width: 1,
                radius: 3,
                color: 'rgb(100, 100, 100)'
            }
        },
        'taskbutton-pressed': {
            style: {
                startColor: 'rgba(180,180,180,0.7)',
                endColor: 'rgba(70, 70, 70, 0.4)',
                startColorPosition: 35,
                endColorPosition: 100,
                width: 1,
                color: 'rgb(100, 100, 100)'
            }
        },
        'taskbutton-pressed-focused': {
            style: {
                startColor: 'rgba(100,100,100,0.7)',
                endColor: 'rgba(0, 0, 0, 0.2)',
                startColorPosition: 35,
                endColorPosition: 100,
                width: 1,
                color: 'rgb(100, 100, 100)'
            }
        },
        'taskbutton-hovered': {
            style: {
                startColor: 'rgba(70,70,70,0.2)',
                endColor: 'rgba(150, 150, 150, 0.7)',
                startColorPosition: 35,
                endColorPosition: 100,
                width: 1,
                color: 'rgb(100, 100, 100)'
            }
        },
        'mainmenu-separator': {
            style: {
                widthTop: 1,
                colorTop: 'rgba(100, 100, 100, 0.7)',
                widthBottom: 1,
                colorBottom: 'rgba(0, 0, 0, 0.2)'
            }
        },
        'mainsubmenu': {
            style: {
                backgroundColor: 'rgba(0, 0, 0, 0.5)'
            }
        },
        "window-captionbar-active" :
        {
          style : {
            width : 1,
            color : "window-border",
            colorBottom : "window-border-caption",
            radius : [5, 5, 0, 0],
            gradientStart : ["window-caption-active-start", 35],
            gradientEnd : ["window-caption-active-end", 100]
          }
        },

        "window-captionbar-inactive" :
        {
          include : "window-captionbar-active",
          style : {
            gradientStart : ["window-caption-inactive-start", 35],
            gradientEnd : ["window-caption-inactive-end", 70]
          }
        }
    }
});
